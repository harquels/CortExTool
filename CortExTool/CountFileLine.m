function [NbLine] = CountFileLine(FID,bEmpty)

% function [NbLine] = CountFileLine(FID,bEmpty)
%
% Counts the number of lines in a file
%
%   INPUTS : - FID:[INT] file identifier (fopen)
%            - bEmpty:[BOOL] if=1, end of file is the 1st empty line
%   OUTPUT : - NbLine:[INT] number of lines

% Copyright (C) 2015  Sylvain Harquel, CNRS
%
% This file is part of CortExTool.
% 
% CortExTool is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% CortExTool is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with CortExTool.  If not, see <http://www.gnu.org/licenses/>

NbLine = -1;
cLine = 1;
fseek(FID,0,-1);
while cLine ~= -1
    cLine = fgetl(FID);
    NbLine = NbLine + 1;
    if bEmpty && isempty(cLine)
        break;
    end
end
fseek(FID,0,-1);