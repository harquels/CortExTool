function [sData, cPatient] = Spike2CETool(cPath)

% function [sData] = Spike2CETool(cPath)
%
% Converts EMG data coming from Spike2 (CED) system into
% matlab structure. Spike2 data must be exported in matlab ".mat" format.
%
%   INPUT : - cPath:[STR] folder containing Spike2 data
%   OUTPUTS : - sData:[STRUCT] matlab structure
%            - cPatient:[STR] patient name

% Copyright (C) 2015  Sylvain Harquel, CNRS
%
% This file is part of CortExTool.
% 
% CortExTool is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% CortExTool is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with CortExTool.  If not, see <http://www.gnu.org/licenses/>

sDir = dir(cPath);
if isempty(sDir) || numel(sDir)==2 || isempty(find(strcmp({sDir.name},'Study.txt')))
    sData = [];
    cPatient = [];
    return
end

% Study file containing patient's name
iStudy = find(strcmp({sDir.name},'Study.txt'));
cPatient = ReadStudyFile([cPath filesep sDir(iStudy).name]);

% Reading data
iStim = 1;
for iDir = 1 : numel(sDir)
    if sDir(iDir).name(1) == '.', continue; end % 1 et 2 = dossiers . et ..
    if sDir(iDir).isdir
        
        cPath_tmp = [cPath filesep sDir(iDir).name];
        
        if strcmp(sDir(iDir).name(end-1:end),'_L') %1: cote droit, 0 : cote gauche
            sData(iStim).Side = 0; %
        elseif strcmp(sDir(iDir).name(end-1:end),'_R')
            sData(iStim).Side = 1; %
        else
            sData(iStim).Side = -1; %
        end
        sData(iStim).cName = sDir(iDir).name;
        
        sChannels = load([cPath_tmp filesep sData(iStim).cName]);
        ceChanName = fieldnames(sChannels);
        
        % Selection of EMG and trigger channels
        if iStim == 1 || strcmp(cChanAns,'No')
            iEMGChan = [];
            iTrigChan = [];
            while numel(iEMGChan)~=1,
                viEMGChan = inputdlg(ceChanName,[sData(iStim).cName ' : EMG channel ?'],[1 40]);
                iEMGChan = setxor([1:5],find(strcmp(viEMGChan,'')));
                if numel(iEMGChan)~=1, waitfor(warndlg('Please select only one EMG channel.')); end
            end
            while numel(iTrigChan)~=1,
                viTrigChan = inputdlg(ceChanName,[sData(iStim).cName ' : Trigger channel ?'],[1 40]);
                iTrigChan = setxor([1:5],find(strcmp(viTrigChan,'')));
                if numel(iTrigChan)~=1, waitfor(warndlg('Please select only one EMG channel.')); end
            end
            if iStim == 1, cChanAns = questdlg('Select the same EMG and trigger channels through all conditions ?', 'Channels', 'Yes', 'No','Yes'); end
        end
        
        eval(['sEMG = sChannels.' ceChanName{iEMGChan} ';']);
        eval(['sTrig = sChannels.' ceChanName{iTrigChan} ';']);
        
        % Sampling freq
        Fs = 1/sEMG.interval;
        
        % Finding trial onsets
        vTrl = find(sTrig.values~=0);
        
        % Time window limits : from -200 to +300ms by
        % default
        vWin = [-0.2 0.3].*Fs;
        vTime = [-0.2 : 1/Fs : 0.3];
        
        % Filling cortextool data structure
        for iTrl = 1 : numel(vTrl)
            
            sData(iStim).ceData{iTrl} = sEMG.values([vWin(1) : vWin(2)]+vTrl(iTrl)).*1e6; %data -> in µV
            sData(iStim).vFs(iTrl) = Fs; % Fs
            sData(iStim).ceTime{iTrl} = vTime; % vecteur temps
            sData(iStim).vOffset(iTrl) = abs(vWin(1)); % offset en sample
            sData(iStim).ceFile{iTrl} = ['<HTML><BODY color="black">' sEMG.title '-' sprintf('%03d',iTrl)]; % Nom fichier EMG
            sData(iStim).vOrder(iTrl) = iTrl; %Ordre chrono de l'EMG
        end
        
        % Next stim (condition)
        iStim = iStim+1;
        
    else
        continue;
    end
end







