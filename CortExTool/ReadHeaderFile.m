function [sParam] = ReadHeaderFile(cFile)

% function [sParam] = ReadHeaderFile(cFile)
%
% Reads header file coming from Keypoint data exportation. Eliminates
% duplication and sort EMGs in chronological order.
%
%   INPUT : - cFile:[STR] header file to read
%   OUTPUT : - sParam:[STRUCT] parameters structure

% Copyright (C) 2015  Sylvain Harquel, CNRS
%
% This file is part of CortExTool.
% 
% CortExTool is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% CortExTool is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with CortExTool.  If not, see <http://www.gnu.org/licenses/>

FID = fopen(cFile);

NbLine = CountFileLine(FID,1);

fgetl(FID);

for iLine = 1 : NbLine-1
    fscanf(FID,'%s',7); % 7 first lines are useless
    sParam(iLine).Offset = str2double(fscanf(FID,'%s',1)); % offset
    sParam(iLine).Fs = str2double(fscanf(FID,'%s',1)); % Samplig freq
    fscanf(FID,'%s',1); % useless
    sParam(iLine).cFile = fscanf(FID,'%s',1); % name of EMG file
end

fclose(FID);

% Sorting EMGs on chronological order
for iFile = 1 : numel(sParam)
   
   % Looking for '--' string chain
   iNb = strfind(sParam(iFile).cFile,'--');
   % Chronological order is either before it ...
   if isempty(str2num(sParam(iFile).cFile(iNb+2)))
       
       % Number with 1 or 2 digit
       if iNb-2 == 0,
           vNum(iFile) = str2num(sParam(iFile).cFile(iNb-1));
       else
           vNum(iFile) = str2num(sParam(iFile).cFile(iNb-2:iNb-1));
       end
       
   % ... or after
   else
       % Up to 4 digits
       if isempty(str2num(sParam(iFile).cFile(iNb+3))),
           vNum(iFile) = str2num(sParam(iFile).cFile(iNb+2));
       elseif isempty(str2num(sParam(iFile).cFile(iNb+4))),
           vNum(iFile) = str2num(sParam(iFile).cFile(iNb+2:iNb+3));
       elseif isempty(str2num(sParam(iFile).cFile(iNb+5))),
           vNum(iFile) = str2num(sParam(iFile).cFile(iNb+2:iNb+4));
       elseif isempty(str2num(sParam(iFile).cFile(iNb+6))),
           vNum(iFile) = str2num(sParam(iFile).cFile(iNb+2:iNb+5));
       end
   end
end
% Sorting in ascending order
[vSort, viSort] = sort(vNum);
sParam = sParam(viSort);


% Duplications suppression
viRm = [];
for iFile = 1 : numel(sParam)-1
    if vSort(iFile) == vSort(iFile+1)
        % If duplication, keep the EMG with the higher suffix number
        iNb = strfind(sParam(iFile).cFile,'__');
        if str2num(sParam(iFile).cFile(iNb+2:iNb+4)) > str2num(sParam(iFile+1).cFile(iNb+2:iNb+4))
            viRm = [viRm (iFile+1)];
        else
            viRm = [viRm iFile];
        end
    end
end

sParam(viRm) = [];
