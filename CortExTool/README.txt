========= CortExTool =========

CortExTool is a signal processing toolbox for measuring cortical
excitability by transcranial magnetic stimulation. CortExTool needs Matlab (The Mathworks Inc.) and the signal processing toolbox to run.

CortExTool is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 
CortExTool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with CortExTool.  If not, see <http://www.gnu.org/licenses/>


========= Installation =========

To install CortExTool, run Matlab and add "CortExTool" folder to your Matlab's path (set path -> add folder).

To run CortExTool, type:
>>CortExTool 
in the Matlab's command window.


========= Help =========

Tutorials and userguide will be soon available on the author's webpage :

http://lpnc.univ-grenoble-alpes.fr/Sylvain-Harquel?lang=fr

Please write to sylvain dot harquel at univ-grenoble-alpes.fr for any
questions, bugs reportings, or future development requests.

