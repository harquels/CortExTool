function [cPatient] = ReadStudyFile(cFile)

% function [cPatient] = ReadStudyFile(cFile)
%
% Reads "Study.txt" file in analysed folder.
%
%   INPUT : - cFile:[STR] file to read
%   OUTPUT : - cPatient:[STR] patient name

% Copyright (C) 2015  Sylvain Harquel, CNRS
%
% This file is part of CortExTool.
% 
% CortExTool is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% CortExTool is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with CortExTool.  If not, see <http://www.gnu.org/licenses/>


FID = fopen(cFile);
fgetl(FID); % Study: ??
fgetl(FID); % StudyGuid: ??

cLine = fgetl(FID); % Patient: name of the patient
cPatient = cLine(9:end);
cPatient(isspace(cPatient)) = [];

fclose(FID);
