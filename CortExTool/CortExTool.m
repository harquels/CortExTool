function varargout = CortExTool(varargin)

% CortExTool is a signal processing toolbox for measuring cortical
% excitability by transcranial magnetic stimulation.
%
% To launch CortExTool, open Matlab and type:
% >>CortExTool (enter)
%
% Users will find all the necessary help in the userguide attached to this
% file. Please write to sylvain dot harquel at univ-grenoble-alpes.Fr for any
% questions, found bugs, or futur developpment requests.

% Copyright (C) 2015-2017  Sylvain Harquel, CNRS
% 
% CortExTool is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% CortExTool is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with CortExTool.  If not, see <http://www.gnu.org/licenses/>


% =========================================================================
% ==================== INIT ===============================================
% =========================================================================

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @MEP_toolbox_OpeningFcn, ...
    'gui_OutputFcn',  @MEP_toolbox_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);

if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end


% --- Executes just before CortExTool is made visible.
function MEP_toolbox_OpeningFcn(hObject, eventdata, handles, varargin)

set(handles.List_Third,'Max',30);
set(handles.List_Third,'Min',0);
set(handles.List_Second,'Max',30);
set(handles.List_Second,'Min',0);
set(handles.Panel_Stat,'Visible','off'); %ind stydy by default

% Globals
global MEP_TOOLBOX_VER
MEP_TOOLBOX_VER = 1;
global SIZE_miEMG
SIZE_miEMG = 6; %1-4: EMG markers, 5: TL index, 6: artifact (-1)

% Default values
sGUI.bMac = ismac;
sGUI.bPc = ispc;
sGUI.bUnix = isunix;
sGUI.sHandles = handles;
sGUI.bSeparate = 0;
sGUI.Sens = 1;
sGUI.CSPThres = 22.5*1e-3;
sGUI.StdMin = 20;
sGUI.AmpMin = 50; % µV
sGUI.LatMin = 0.015; % s
sGUI.bGroup = 0;
sGUI.iBslCond = [];
sGUI.cCalcMeasure = 'Amp';
sGUI.cCalcMethod = 'Raw';
sGUI.bShowBsl = 0;
sGUI.ceSide = {[],[]}; % Condition order, by side (L, R)
sGUI.cFeatPlot = 'Amp';
set(sGUI.sHandles.Radio_FtAmp,'Value',1);
sGUI.ArtfctThres = 500; %microV
sGUI.bExcludeArtfct = 0;
sGUI.cDataType = '';

ceCTPath = which('CortExTool.m','-ALL');
if numel(ceCTPath) > 1, 
    waitfor(warndlg('You have more than one CortExTool version in your matlab path. Please clean your path and restart CortExTool.'));
end

% Dafault values display
set(handles.Edit_Sens,'String',num2str(sGUI.Sens));
set(handles.Edit_StdMin,'String',num2str(sGUI.StdMin));
set(handles.Edit_AmpMin,'String',num2str(sGUI.AmpMin));
set(handles.CSPThres,'String',num2str(sGUI.CSPThres*1000));
set(handles.Slide_CSP_Sens,'Value',(sGUI.CSPThres*1e3)/200);


sGUI.cCTPath = ceCTPath{1}(1:end-12);

guidata(hObject, sGUI);
end


function varargout = MEP_toolbox_OutputFcn(hObject, eventdata, handles)

end




% =========================================================================
% ==================== MENU ENTRIES =======================================
% =========================================================================

function Menu_Dataset_Callback(hObject, eventdata, handles)

end

% == Open and load Keypoint data
function Menu_OpenKeyPt_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
sData = [];

[cPath] = uigetdir('Please select a data folder');
[sData, cPatient] = KeyPt2CETool(cPath);

if isempty(sData),
    errordlg('Selected folder does not contain readable data','Error loading data');
    
else
    
    set(sGUI.sHandles.Text_Path,'String',cPath(max(1,end-40):end));
    set(sGUI.sHandles.Text_Patient,'String',cPath(max(strfind(cPath,filesep))+1:end));
    set(sGUI.sHandles.List_Second,'String',{});
    set(sGUI.sHandles.List_Second,'Value',[]);
    set(sGUI.sHandles.List_Third,'String',{});
    set(sGUI.sHandles.List_Third,'Value',[]);
    set(sGUI.sHandles.List_Third,'Max',2000);
    set(sGUI.sHandles.List_Third,'Min',0);
    
    set(sGUI.sHandles.Panel_EMG,'Visible','on');
    set(sGUI.sHandles.Panel_Res,'Visible','on');
    set(sGUI.sHandles.Panel_Trig,'Visible','on');
    set(sGUI.sHandles.Panel_Stat,'Visible','off');
    set(sGUI.sHandles.text4,'Visible','on');
    set(sGUI.sHandles.Push_Right,'Visible','on');
    set(sGUI.sHandles.Push_Left,'Visible','on');
    set(sGUI.sHandles.Push_Both,'Visible','on');

    cla(sGUI.sHandles.Axes,'reset');
    
    sGUI.cPath = cPath;
    sGUI.ceSide = {[],[]};
    sGUI.bGroup = 0;
    sGUI.cDataType = 'KeyPt';

    [sGUI.sData sGUI.sEMG sGUI.ceSide] = LoadData(sGUI.cPath,sData);
end

guidata(hObject,sGUI);

end


% == Open and load Spike2 data
function Menu_OpenSp2_Callback(hObject, eventdata, handles)


sGUI = guidata(hObject);
sData = [];

[cPath] = uigetdir('Please select a data folder');
[sData, cPatient] = Data2CETool(cPath,'Sp2');

if isempty(sData),
    errordlg('Selected folder does not contain readable data','Error loading data');
    
else
    
    set(sGUI.sHandles.Text_Path,'String',cPath(max(1,end-40):end));
    set(sGUI.sHandles.Text_Patient,'String',cPath(max(strfind(cPath,filesep))+1:end));
    set(sGUI.sHandles.Text_Chan,'String',sData(1).cEMGChan);
    set(sGUI.sHandles.List_Second,'String',{});
    set(sGUI.sHandles.List_Second,'Value',[]);
    set(sGUI.sHandles.List_Third,'String',{});
    set(sGUI.sHandles.List_Third,'Value',[]);
    set(sGUI.sHandles.List_Third,'Max',2000);
    set(sGUI.sHandles.List_Third,'Min',0);
    
    % Visibilité : groupe/ind
    set(sGUI.sHandles.Panel_EMG,'Visible','on');
    set(sGUI.sHandles.Panel_Res,'Visible','on');
    set(sGUI.sHandles.Panel_Trig,'Visible','on');
    set(sGUI.sHandles.Panel_Stat,'Visible','off');
    set(sGUI.sHandles.text4,'Visible','on');
    set(sGUI.sHandles.Push_Right,'Visible','on');
    set(sGUI.sHandles.Push_Left,'Visible','on');
    set(sGUI.sHandles.Push_Both,'Visible','on');

    
    cla(sGUI.sHandles.Axes,'reset');
    
    sGUI.cPath = cPath;
    sGUI.ceSide = {[],[]};
    sGUI.bGroup = 0;
    sGUI.cDataType = 'Sp2';

    [sGUI.sData sGUI.sEMG sGUI.ceSide] = LoadData(sGUI.cPath,sData);
end

guidata(hObject,sGUI);

end


function Menu_OpenEDF_Callback(hObject, eventdata, handles)
sGUI = guidata(hObject);
sData = [];

[cPath] = uigetdir('Please select a data folder');
[sData, cPatient] = Data2CETool(cPath,'edf');

if isempty(sData),
    errordlg('Selected folder does not contain readable data','Error loading data');
    
else
    
    set(sGUI.sHandles.Text_Path,'String',cPath(max(1,end-40):end));
    set(sGUI.sHandles.Text_Patient,'String',cPath(max(strfind(cPath,filesep))+1:end));
    set(sGUI.sHandles.List_Second,'String',{});
    set(sGUI.sHandles.List_Second,'Value',[]);
    set(sGUI.sHandles.List_Third,'String',{});
    set(sGUI.sHandles.List_Third,'Value',[]);
    set(sGUI.sHandles.List_Third,'Max',2000);
    set(sGUI.sHandles.List_Third,'Min',0);
    
    % Visibilité : groupe/ind
    set(sGUI.sHandles.Panel_EMG,'Visible','on');
    set(sGUI.sHandles.Panel_Res,'Visible','on');
    set(sGUI.sHandles.Panel_Trig,'Visible','on');
    set(sGUI.sHandles.Panel_Stat,'Visible','off');
    set(sGUI.sHandles.text4,'Visible','on');
    set(sGUI.sHandles.Push_Right,'Visible','on');
    set(sGUI.sHandles.Push_Left,'Visible','on');
    set(sGUI.sHandles.Push_Both,'Visible','on');

    
    cla(sGUI.sHandles.Axes,'reset');
    
    sGUI.cPath = cPath;
    sGUI.ceSide = {[],[]};
    sGUI.bGroup = 0;
    sGUI.cDataType = 'edf';

    [sGUI.sData sGUI.sEMG sGUI.ceSide] = LoadData(sGUI.cPath,sData);
end

guidata(hObject,sGUI);

end


function Menu_OpenMat_Callback(hObject, eventdata, handles)
sGUI = guidata(hObject);
sData = [];

[cPath] = uigetdir('Please select a data folder');
[sData, cPatient] = Data2CETool(cPath,'mat');

if isempty(sData),
    errordlg('Selected folder does not contain readable data','Error loading data');
    
else
    
    set(sGUI.sHandles.Text_Path,'String',cPath(max(1,end-40):end));
    set(sGUI.sHandles.Text_Patient,'String',cPath(max(strfind(cPath,filesep))+1:end));
    set(sGUI.sHandles.List_Second,'String',{});
    set(sGUI.sHandles.List_Second,'Value',[]);
    set(sGUI.sHandles.List_Third,'String',{});
    set(sGUI.sHandles.List_Third,'Value',[]);
    set(sGUI.sHandles.List_Third,'Max',2000);
    set(sGUI.sHandles.List_Third,'Min',0);
    
    % Visibilité : groupe/ind
    set(sGUI.sHandles.Panel_EMG,'Visible','on');
    set(sGUI.sHandles.Panel_Res,'Visible','on');
    set(sGUI.sHandles.Panel_Trig,'Visible','on');
    set(sGUI.sHandles.Panel_Stat,'Visible','off');
    set(sGUI.sHandles.text4,'Visible','on');
    set(sGUI.sHandles.Push_Right,'Visible','on');
    set(sGUI.sHandles.Push_Left,'Visible','on');
    set(sGUI.sHandles.Push_Both,'Visible','on');

    
    cla(sGUI.sHandles.Axes,'reset');
    
    sGUI.cPath = cPath;
    sGUI.ceSide = {[],[]};
    sGUI.bGroup = 0;
    sGUI.cDataType = 'mat';

    [sGUI.sData sGUI.sEMG sGUI.ceSide] = LoadData(sGUI.cPath,sData);
end

guidata(hObject,sGUI);
end

% Open data from LabView export - TO DO !!! 
function Menu_OpenLabV_Callback(hObject, eventdata, handles)
sGUI = guidata(hObject);
sData = [];

[cPath] = uigetdir('Please select a data folder');
[sData, cPatient] = Data2CETool(cPath,'LabV');

if isempty(sData),
    errordlg('Selected folder does not contain readable data','Error loading data');
    
else
    
    set(sGUI.sHandles.Text_Path,'String',cPath(max(1,end-40):end));
    set(sGUI.sHandles.Text_Patient,'String',cPath(max(strfind(cPath,filesep))+1:end));
    set(sGUI.sHandles.List_Second,'String',{});
    set(sGUI.sHandles.List_Second,'Value',[]);
    set(sGUI.sHandles.List_Third,'String',{});
    set(sGUI.sHandles.List_Third,'Value',[]);
    set(sGUI.sHandles.List_Third,'Max',2000);
    set(sGUI.sHandles.List_Third,'Min',0);
    
    % Visibilité : groupe/ind
    set(sGUI.sHandles.Panel_EMG,'Visible','on');
    set(sGUI.sHandles.Panel_Res,'Visible','on');
    set(sGUI.sHandles.Panel_Trig,'Visible','on');
    set(sGUI.sHandles.Panel_Stat,'Visible','off');
    set(sGUI.sHandles.text4,'Visible','on');
    set(sGUI.sHandles.Push_Right,'Visible','on');
    set(sGUI.sHandles.Push_Left,'Visible','on');
    set(sGUI.sHandles.Push_Both,'Visible','on');

    
    cla(sGUI.sHandles.Axes,'reset');
    
    sGUI.cPath = cPath;
    sGUI.ceSide = {[],[]};
    sGUI.bGroup = 0;
    sGUI.cDataType = 'LabV';

    [sGUI.sData sGUI.sEMG sGUI.ceSide] = LoadData(sGUI.cPath,sData);
end

guidata(hObject,sGUI);
end


% == Open and load group study from Keypoint data
function Menu_Open_Grp_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
sGUI.ceData = [];
sGUI.ceEMG = [];
sGUI.cePatient = [];

[sGUI.cPath] = uigetdir('Please select a folder containing all subjects');
sDir = dir(sGUI.cPath);


% Look for a group study file...
bLoaded = 0;
for iDir = 3 : numel(sDir)
    if ~isempty(strfind(sDir(iDir).name,'_GrpStudy.mat'))
        load([sGUI.cPath filesep sDir(iDir).name]);
        sGUI.ceData = ceData;
        sGUI.ceDataBckp = ceDataBckp;
        sGUI.ceEMG = ceEMG;
        sGUI.cePatient = cePatient;
        sGUI.ceCond = ceCond;
        sGUI.ceCondBckp = ceCondBckp;
        bLoaded = 1;
    end
end
% ... if not available, load data from scratch
if ~bLoaded
    hWT = waitbar(0,'Scanning folder...');
    for iDir = 3 : numel(sDir)
        if sDir(iDir).isdir
            [sData, cPatient] = KeyPt2CETool([sGUI.cPath filesep sDir(iDir).name]);
            if ~isempty(sData),
                sGUI.ceData{end+1} = sData;
                sGUI.cePatient{end+1} = cPatient;
                [sGUI.ceData{end} sGUI.ceEMG{end+1}] = LoadData([sGUI.cPath filesep sDir(iDir).name],sGUI.ceData{end});
            end
        end
        waitbar(iDir/(numel(sDir)-2),hWT);
    end
    close(hWT);
end

if isempty(sGUI.ceData),
    errordlg('Selected folder does not contain group study data','Error loading data');
else
    sGUI.bGroup = 1;
    sGUI.iBslCond = [];
    set(sGUI.sHandles.List_Second,'String',{});
    set(sGUI.sHandles.List_Second,'Value',[]);
    set(sGUI.sHandles.List_Third,'String',{});
    set(sGUI.sHandles.List_Third,'Value',[]);
    set(sGUI.sHandles.List_Third,'Max',1);
    set(sGUI.sHandles.List_Third,'Min',0);
    cla(sGUI.sHandles.Axes,'reset');

    set(sGUI.sHandles.Panel_EMG,'Visible','off');
    set(sGUI.sHandles.Panel_Res,'Visible','off');
    set(sGUI.sHandles.Panel_Trig,'Visible','off');
    set(sGUI.sHandles.Panel_Stat,'Visible','on');
    set(sGUI.sHandles.text4,'Visible','off');
    set(sGUI.sHandles.Push_Right,'Visible','off');
    set(sGUI.sHandles.Push_Left,'Visible','off');
    set(sGUI.sHandles.Push_Both,'Visible','off');
    
    set(sGUI.sHandles.Text_Path,'String',sGUI.cPath(max(1,end-40):end));
    set(sGUI.sHandles.Text_Patient,'String','-- Group study mode --');
    
    if ~bLoaded
        [sGUI.ceData sGUI.ceCond] = SortCond(sGUI.ceData);
        sGUI.ceDataBckp = sGUI.ceData;
        sGUI.ceCondBckp = sGUI.ceCond;
    end
    set(sGUI.sHandles.List_Second,'String',sGUI.ceCond(1,:));
    
end

guidata(hObject,sGUI);

end


% == Open and load group study from Spike2 data
function Menu_Open_Sp2Grp_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
sGUI.ceData = [];
sGUI.ceEMG = [];
sGUI.cePatient = [];

[sGUI.cPath] = uigetdir('Please select a folder containing all subjects');
sDir = dir(sGUI.cPath);

% Look for a group study file...
bLoaded = 0;
for iDir = 3 : numel(sDir)
    if ~isempty(strfind(sDir(iDir).name,'_GrpStudy.mat'))
        load([sGUI.cPath filesep sDir(iDir).name]);
        sGUI.ceData = ceData;
        sGUI.ceDataBckp = ceDataBckp;
        sGUI.ceEMG = ceEMG;
        sGUI.cePatient = cePatient;
        sGUI.ceCond = ceCond;
        sGUI.ceCondBckp = ceCondBckp;
        bLoaded = 1;
    end
end
% ... if not available, load data from scratch
if ~bLoaded
    hWT = waitbar(0,'Scanning folder...');
    for iDir = 3 : numel(sDir)
        if sDir(iDir).isdir
            [sData, cPatient] = Data2CETool(cPath,'Sp2');
            if ~isempty(sData),
                sGUI.ceData{end+1} = sData;
                sGUI.cePatient{end+1} = cPatient;
                [sGUI.ceData{end} sGUI.ceEMG{end+1}] = LoadData([sGUI.cPath filesep sDir(iDir).name],sGUI.ceData{end});
            end
        end
        waitbar(iDir/(numel(sDir)-2),hWT);
    end
    close(hWT);
end

if isempty(sGUI.ceData),
    errordlg('Selected folder does not contain group study data','Error loading data');
else
    sGUI.bGroup = 1;
    sGUI.iBslCond = [];
    set(sGUI.sHandles.List_Second,'String',{});
    set(sGUI.sHandles.List_Second,'Value',[]);
    set(sGUI.sHandles.List_Third,'String',{});
    set(sGUI.sHandles.List_Third,'Value',[]);
    set(sGUI.sHandles.List_Third,'Max',1);
    set(sGUI.sHandles.List_Third,'Min',0);
    cla(sGUI.sHandles.Axes,'reset');

    set(sGUI.sHandles.Panel_EMG,'Visible','off');
    set(sGUI.sHandles.Panel_Res,'Visible','off');
    set(sGUI.sHandles.Panel_Trig,'Visible','off');
    set(sGUI.sHandles.Panel_Stat,'Visible','on');
    set(sGUI.sHandles.text4,'Visible','off');
    set(sGUI.sHandles.Push_Right,'Visible','off');
    set(sGUI.sHandles.Push_Left,'Visible','off');
    set(sGUI.sHandles.Push_Both,'Visible','off');
    
    if ~bLoaded
        [sGUI.ceData sGUI.ceCond] = SortCond(sGUI.ceData);
        sGUI.ceDataBckp = sGUI.ceData;
        sGUI.ceCondBckp = sGUI.ceCond;
    end
    set(sGUI.sHandles.List_Second,'String',sGUI.ceCond(1,:));
    
end

guidata(hObject,sGUI);


end


% == Open and load group study from EDF data
function Menu_Open_EDFGrp_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
sGUI.ceData = [];
sGUI.ceEMG = [];
sGUI.cePatient = [];

[sGUI.cPath] = uigetdir('Please select a folder containing all subjects');
sDir = dir(sGUI.cPath);

% Look for a group study file...
bLoaded = 0;
for iDir = 3 : numel(sDir)
    if ~isempty(strfind(sDir(iDir).name,'_GrpStudy.mat'))
        load([sGUI.cPath filesep sDir(iDir).name]);
        sGUI.ceData = ceData;
        sGUI.ceDataBckp = ceDataBckp;
        sGUI.ceEMG = ceEMG;
        sGUI.cePatient = cePatient;
        sGUI.ceCond = ceCond;
        sGUI.ceCondBckp = ceCondBckp;
        bLoaded = 1;
    end
end
% ... if not available, load data from scratch
if ~bLoaded
    hWT = waitbar(0,'Scanning folder...');
    for iDir = 3 : numel(sDir)
        if sDir(iDir).isdir
            [sData, cPatient] = Data2CETool(cPath,'edf');
            if ~isempty(sData),
                sGUI.ceData{end+1} = sData;
                sGUI.cePatient{end+1} = cPatient;
                [sGUI.ceData{end} sGUI.ceEMG{end+1}] = LoadData([sGUI.cPath filesep sDir(iDir).name],sGUI.ceData{end});
            end
        end
        waitbar(iDir/(numel(sDir)-2),hWT);
    end
    close(hWT);
end

if isempty(sGUI.ceData),
    errordlg('Selected folder does not contain group study data','Error loading data');
else
    sGUI.bGroup = 1;
    sGUI.iBslCond = [];
    set(sGUI.sHandles.List_Second,'String',{});
    set(sGUI.sHandles.List_Second,'Value',[]);
    set(sGUI.sHandles.List_Third,'String',{});
    set(sGUI.sHandles.List_Third,'Value',[]);
    set(sGUI.sHandles.List_Third,'Max',1);
    set(sGUI.sHandles.List_Third,'Min',0);
    cla(sGUI.sHandles.Axes,'reset');

    set(sGUI.sHandles.Panel_EMG,'Visible','off');
    set(sGUI.sHandles.Panel_Res,'Visible','off');
    set(sGUI.sHandles.Panel_Trig,'Visible','off');
    set(sGUI.sHandles.Panel_Stat,'Visible','on');
    set(sGUI.sHandles.text4,'Visible','off');
    set(sGUI.sHandles.Push_Right,'Visible','off');
    set(sGUI.sHandles.Push_Left,'Visible','off');
    set(sGUI.sHandles.Push_Both,'Visible','off');
    
    if ~bLoaded
        [sGUI.ceData sGUI.ceCond] = SortCond(sGUI.ceData);
        sGUI.ceDataBckp = sGUI.ceData;
        sGUI.ceCondBckp = sGUI.ceCond;
    end
    set(sGUI.sHandles.List_Second,'String',sGUI.ceCond(1,:));
    
end

guidata(hObject,sGUI);



end

% == Reload selected data from scratch
function Menu_Reload_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
sGUI.sData = [];
sGUI.sEMG = [];

switch sGUI.cDataType
    case 'KeyPt'
        [sGUI.sData, sGUI.cPatient] = KeyPt2CETool(sGUI.cPath);
    otherwise
        [sGUI.sData, sGUI.cPatient] = Data2CETool(sGUI.cPath,sGUI.cDataType);
end

if isempty(sGUI.sData),
    errordlg('Selected folder does not contain readable data','Error loading data');
    
else
    
    set(sGUI.sHandles.Text_Path,'String',sGUI.cPath(max(1,end-40):end));
    set(sGUI.sHandles.Text_Patient,'String',sGUI.cPath(max(strfind(sGUI.cPath,filesep))+1:end));
    set(sGUI.sHandles.List_Second,'String',{});
    set(sGUI.sHandles.List_Third,'String',{});
    cla(sGUI.sHandles.Axes,'reset');
    set(sGUI.sHandles.Panel_EMG,'Visible','on');
    set(sGUI.sHandles.Panel_Stat,'Visible','off');
    
    sGUI.bGroup = 0;
    sGUI.ceSide = {[],[]};
    
    [sGUI.sData sGUI.sEMG sGUI.ceSide] = LoadData(sGUI.cPath,sGUI.sData);
end

guidata(hObject,sGUI);

end



function Menu_Template_Callback(hObject, eventdata, handles)

end

% == Choose EMG template to be used. Previous template is save with a
% suffix '_old_' + time 
function Menu_ChooseTemplate_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

[cFile, cPath] = uigetfile({'*.mat'},'Please select an EMG template file','MATLAB mat Files (*.mat)');

if cFile~=0
     
    vC = clock;
    cSuffix = [num2str(vC(1)) num2str(vC(2)) num2str(vC(3)) num2str(vC(4)) num2str(vC(5)) num2str(vC(6))]; % time
    movefile([sGUI.cCTPath filesep 'EMG_template.mat'],[sGUI.cCTPath filesep 'EMG_template_old' cSuffix '.mat']);
    copyfile([cPath filesep cFile], [sGUI.cCTPath filesep 'EMG_template.mat']);
    
    guidata(hObject,sGUI);
    
end

end

% == Generate EMG template from selected EMGs. EMGs are epoched with a time
% window surrounding Peak_1, and aligned according to it.
function Menu_GenerateTemplate_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

TEMPLATESIZE = 0.08; % template size of 80ms
FS = sGUI.sData(sGUI.viSide(sGUI.viStim(1))).vFs(1);

% User choose the size of the time window
ceAns = inputdlg('Time of interest (in ms) surrounding Peak_1','Time window specification',[1 40]);
TimeWin = str2num(cell2mat(ceAns))*10^(-3) * FS;


mEMG = [];
if numel(sGUI.viStim) == 1
    for iEMG = sGUI.viEMG
        Peak1 = sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(1,iEMG);
        mEMG = [mEMG ; sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData{iEMG}(Peak1 - TimeWin : Peak1 + TimeWin)'];
    end
    else
    for iStim = sGUI.viStim
        for iEMG = 1 : numel(sGUI.sData(sGUI.viSide(iStim)).ceData)
            Peak1 = sGUI.sEMG(sGUI.viSide(iStim)).miEMG(1,iEMG);
            mEMG = [mEMG ; sGUI.sData(sGUI.viSide(iStim)).ceData{iEMG}(Peak1 - TimeWin : Peak1 + TimeWin)'];
        end   
    end
end

mEMG = [zeros(size(mEMG,1),floor((TEMPLATESIZE*FS-2*TimeWin)/2)) mEMG zeros(size(mEMG,1),floor((TEMPLATESIZE*FS-2*TimeWin)/2))];

[cFile, cPath] = uiputfile({'*.mat'},'Save EMG template as...','.mat');

if cFile~=0
    save([cPath cFile],'mEMG','FS');
    guidata(hObject,sGUI);
end

end

% == Display the EMG template currently used
function Menu_DisplayTemplate_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

ceEMG_template = which('EMG_template.mat','-ALL');
if numel(ceEMG_template) > 1
    warning(['Multiple occurence of EMG_template.mat file found ==> Please clean your matlab path. Loading ' ceEMG_template{1}]);
end
load(ceEMG_template{1});

try
    vEMG_tmp = mean(mEMG,1); % template EMG
catch
   errordlg('Invalid EMG template file. Please choose a valid file.'); 
   return
end

hFigure = sGUI.sHandles.figure1;
PlotEMG({[1:numel(vEMG_tmp)]},{vEMG_tmp},{'EMG Template'},[],hFigure,1);

end


% =========================================================================
% ==================== LISTING EMGs / CONDITIONS ==========================
% =========================================================================

% == Display any conditions found on the left side
function Push_Left_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

sGUI.viSide = find([sGUI.sData.Side] == 0); % 1: R, 0: L, 3: both
if isempty(sGUI.viSide),
    warndlg('No stimulation found for the left side, showing RIGHT side');
    sGUI.Side = 1;
else
    sGUI.Side = 0;
end

if isempty(sGUI.ceSide{sGUI.Side+1}),
    sGUI.viSide = find([sGUI.sData.Side] == sGUI.Side);
else
    sGUI.viSide = sGUI.ceSide{sGUI.Side+1};
end

sGUI.viStim = [];
set(sGUI.sHandles.List_Second,'String',{sGUI.sData(sGUI.viSide).cName});
set(sGUI.sHandles.List_Second,'Value',1);
set(sGUI.sHandles.List_Third,'String',{});
set(sGUI.sHandles.List_Third,'Value',1);

guidata(hObject,sGUI);

end


% == Display any conditions found on the right side
function Push_Right_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

sGUI.viSide = find([sGUI.sData.Side] == 1); % 1: R, 0: L, 3: both
if isempty(sGUI.viSide),
    warndlg('No stimulation found for the right side, showing LEFT side');
    sGUI.Side = 0;
else
    sGUI.Side = 1;
end

if isempty(sGUI.ceSide{sGUI.Side+1}),
    sGUI.viSide = find([sGUI.sData.Side] == sGUI.Side);
else
    sGUI.viSide = sGUI.ceSide{sGUI.Side+1};
end

sGUI.viStim = [];
set(sGUI.sHandles.List_Second,'String',{sGUI.sData(sGUI.viSide).cName});
set(sGUI.sHandles.List_Second,'Value',1);
set(sGUI.sHandles.List_Third,'String',{});
set(sGUI.sHandles.List_Third,'Value',1);

guidata(hObject,sGUI);
end

% ==  Display all conditions found
function Push_Both_Callback(hObject, eventdata, handles)
sGUI = guidata(hObject);

sGUI.viSide = [1 : numel(sGUI.sData)];

sGUI.Side = 3;

if isempty(sGUI.ceSide{1}) && isempty(sGUI.ceSide{2}),
   sGUI.viSide = [1 : numel(sGUI.sData)];
elseif  ~isempty(sGUI.ceSide{1}) && isempty(sGUI.ceSide{2}),
    % Follow re-defined order
    sGUI.viSide = [sGUI.ceSide{1}];
    % Add other EMGs
    sGUI.viSide = [sGUI.viSide setxor([1 : numel(sGUI.sData)], sGUI.viSide)];
    
elseif isempty(sGUI.ceSide{1}) && ~isempty(sGUI.ceSide{2}),
    sGUI.viSide = [sGUI.ceSide{2}];
    sGUI.viSide = [sGUI.viSide setxor([1 : numel(sGUI.sData)], sGUI.viSide)];
else
    sGUI.viSide = [sGUI.ceSide{1} sGUI.ceSide{2}];
    sGUI.viSide = [sGUI.viSide setxor([1 : numel(sGUI.sData)], sGUI.viSide)];
end

sGUI.viStim = [];
set(sGUI.sHandles.List_Second,'String',{sGUI.sData(sGUI.viSide).cName});
set(sGUI.sHandles.List_Second,'Value',1);
set(sGUI.sHandles.List_Third,'String',{});
set(sGUI.sHandles.List_Third,'Value',1);

guidata(hObject,sGUI);
end


% == 2nd column (first being Left/Right/Both): listing conditions
function List_Second_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

if ~sGUI.bGroup

    sGUI.viStim = get(hObject,'value');
    
    % If one condition is selected, automatic selection and plotting of corresponding EMGs
    if numel(sGUI.viStim) == 1
        
        set(sGUI.sHandles.List_Third,'String',sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile);
        set(sGUI.sHandles.List_Third,'Value',1:numel(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile));
        
        % Ploting
        hFigure = sGUI.sHandles.figure1;
        sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime,sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData,...
        sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile,sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG,hFigure,1,sGUI.sData(sGUI.viSide(sGUI.viStim)).vFs);
        % Selection
        sGUI.viEMG = [1 : numel(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile)];
        
    else
        set(sGUI.sHandles.List_Third,'String',{});
        set(sGUI.sHandles.List_Third,'Value',1);
    end

else
    
end

guidata(hObject,sGUI);

end
function List_Second_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


% == 3rd column: listing EMGs
function List_Third_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

% If individual data...
if ~sGUI.bGroup

    %... Plot the selected EMGs
    sGUI.viEMG = get(hObject,'value');
    
    hFigure = sGUI.sHandles.figure1;
    sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
        sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),hFigure,1);
    
    % TL index
    set(sGUI.sHandles.Text_MEP_Q,'String',[' TL = ' num2str(nanmean(sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(5,sGUI.viEMG)))]);

% If group data...
else
   
    %... Plot feature means for the selected subject
    sGUI.iPatient = get(hObject,'value');
    
    hFigure = sGUI.sHandles.figure1;
    
    cla(sGUI.sHandles.Axes,'reset');
    switch sGUI.cCalcMeasure
            case 'Amp'
                cYlabel = 'Amplitude (µV)';
            case 'TLi'
                cYlabel = 'TL index ( %)';
            case 'RMS'
                cYlabel = 'RMS (µV)';
            case 'Lat'
                cYlabel = 'Latency (ms)';
            case 'Dur'
                cYlabel = 'Duration (ms)';
    end
    if ~isempty(sGUI.iBslCond), cYlabel(end-3:end+6) = '(% of bsl)'; end
   
    % Showing baseline if needed
    if sGUI.bShowBsl, 
        viCond = [1:length(sGUI.ceCond)]; else viCond = setxor([1:length(sGUI.ceCond)],sGUI.iBslCond); end
    sGUI.hFigure = PlotBar(sGUI.sIndStat(sGUI.iPatient).mMeanStd(1,viCond),sGUI.sIndStat(sGUI.iPatient).mMeanStd(2,viCond),...
        'b',sGUI.ceCond(1,viCond),[],hFigure,[],cYlabel);
    
    
end

guidata(hObject,sGUI);

end
function List_Third_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% == Move selected conditions up
function Push_Up_Callback(hObject, eventdata, handles)


sGUI = guidata(hObject);

if ~sGUI.bGroup
    
    viStim_new = sGUI.viStim - 1;
    
    if all(viStim_new ~= 0)
        
        for iStim = 1 : numel(viStim_new)
            sGUI.viSide(viStim_new(iStim):end) = sGUI.viSide([viStim_new(iStim)+1 viStim_new(iStim) viStim_new(iStim)+2:end]);
        end
        
        sGUI.viStim = viStim_new;
        
        set(sGUI.sHandles.List_Second,'String',{sGUI.sData(sGUI.viSide).cName});
        set(sGUI.sHandles.List_Second,'Value',sGUI.viStim);
        
        sGUI.ceSide{sGUI.Side+1} = sGUI.viSide;
        guidata(hObject,sGUI);
        
    end
    
else
    
    viCond = get(sGUI.sHandles.List_Second,'value');
    viCond_new = viCond - 1;
    
    if all(viCond_new ~= 0)
        for iCond = 1 : numel(viCond_new)
            sGUI.ceCond(:,viCond_new(iCond):end) = sGUI.ceCond(:,[viCond_new(iCond)+1 viCond_new(iCond) viCond_new(iCond)+2:end]);
            if sGUI.iBslCond == viCond(iCond)-1
                sGUI.iBslCond = sGUI.iBslCond+1;
            elseif sGUI.iBslCond == viCond(iCond)
                sGUI.iBslCond = sGUI.iBslCond-1;
            end
            if isfield(sGUI,'sIndStat'),
                for iStat =  1 : numel(sGUI.sIndStat)
                    sGUI.sIndStat(iStat).mMeanStd(:,viCond_new(iCond):end) = sGUI.sIndStat(iStat).mMeanStd(:,[viCond_new(iCond)+1 viCond_new(iCond) viCond_new(iCond)+2:end]);
                    sGUI.sIndStat(iStat).ceTrl(viCond_new(iCond):end) = sGUI.sIndStat(iStat).ceTrl([viCond_new(iCond)+1 viCond_new(iCond) viCond_new(iCond)+2:end]);
                end
            end
        end
        set(sGUI.sHandles.List_Second, 'String', sGUI.ceCond(1,:));
        set(sGUI.sHandles.List_Second,'Value',viCond_new);
        set(sGUI.sHandles.List_Third,'String',{});
        set(sGUI.sHandles.List_Third,'Value',1);
        guidata(hObject,sGUI);
    end
    
end

end

% == Move selected conditions down
function Push_Down_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

if ~sGUI.bGroup
    
    viStim_new = sGUI.viStim + 1;
    
    if all(viStim_new <= numel(sGUI.viSide))
        
        for iStim = numel(viStim_new) : -1 : 1
            sGUI.viSide(1:viStim_new(iStim)) = sGUI.viSide([1:viStim_new(iStim)-2 viStim_new(iStim) viStim_new(iStim)-1]);
        end
        
        sGUI.viStim = viStim_new;
        
        set(sGUI.sHandles.List_Second,'String',{sGUI.sData(sGUI.viSide).cName});
        set(sGUI.sHandles.List_Second,'Value',sGUI.viStim);
        
        sGUI.ceSide{sGUI.Side+1} = sGUI.viSide;
        
        guidata(hObject,sGUI);
    end
else
    viCond = get(sGUI.sHandles.List_Second,'value');
    viCond_new = viCond + 1;
    
    if all(viCond_new <= size(sGUI.ceCond,2))
        for iCond = numel(viCond_new)  : -1 : 1
            sGUI.ceCond(:,1:viCond_new(iCond)) = sGUI.ceCond(:,[1:viCond_new(iCond)-2 viCond_new(iCond) viCond_new(iCond)-1]);
            if sGUI.iBslCond == viCond(iCond)+1
                sGUI.iBslCond = sGUI.iBslCond-1;
            elseif sGUI.iBslCond == viCond(iCond)
                sGUI.iBslCond = sGUI.iBslCond+1;
            end
            if isfield(sGUI,'sIndStat'),
                for iStat =  1 : numel(sGUI.sIndStat)
                    sGUI.sIndStat(iStat).mMeanStd(:,1:viCond_new(iCond)) = sGUI.sIndStat(iStat).mMeanStd(:,[1:viCond_new(iCond)-2 viCond_new(iCond) viCond_new(iCond)-1]); 
                    sGUI.sIndStat(iStat).ceTrl(1:viCond_new(iCond)) = sGUI.sIndStat(iStat).ceTrl([1:viCond_new(iCond)-2 viCond_new(iCond) viCond_new(iCond)-1]);
                end
            end
        end
            
        set(sGUI.sHandles.List_Second, 'String', sGUI.ceCond(1,:));
        set(sGUI.sHandles.List_Second,'Value',viCond_new);
        set(sGUI.sHandles.List_Third,'String',{});
        set(sGUI.sHandles.List_Third,'Value',1);
        guidata(hObject,sGUI);
    end
end

end


% =========================================================================
% ==================== INDIVIDUAL SUBJ. PANEL =============================
% =========================================================================



% =========================================================================
% ==================== 1. PREPROCESSING PANEL =============================
% =========================================================================

% =================== a. ARTIFACTS ========================================

% ==Automatic artifacts detection
function Push_Artfct_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

for iStim = sGUI.viStim
    
    % Apply on EMGs or condition ?
    if numel(sGUI.viStim) ==1, viData = sGUI.viEMG; else viData = [1 : numel(sGUI.sData(sGUI.viSide(iStim)).ceData)]; end
    for iData = viData
        
        vTmp = sGUI.sData(sGUI.viSide(iStim)).ceData{iData}(1 : sGUI.sData(sGUI.viSide(iStim)).vOffset(iData));
        
        %!!TO DO --
        [N, Wn] = buttord(80/(sGUI.sData(iData).vFs(1)/2), 100/(sGUI.sData(iData).vFs(1)/2), 3, 20);
        [B,A] = butter(N,Wn,'high');
        vTmp = filtfilt(B,A,double(vTmp));
        
        vRMS = sqrt(mean(vTmp).^2 + std(vTmp).^2); %RMS de baseline
        
        % Artifact if RMS above threshold and not previously detected
        if vRMS > sGUI.ArtfctThres && sGUI.sEMG(sGUI.viSide(iStim)).miEMG(6,iData) ~= -1,
            % Red display
            sGUI.sData(sGUI.viSide(iStim)).ceFile{iData} = regexprep(sGUI.sData(sGUI.viSide(iStim)).ceFile{iData},'"black"', '"red"');
            % Set as artifact in miEMG
            sGUI.sEMG(sGUI.viSide(iStim)).miEMG(6,iData) = -1;
            
        % Not Artifact if RMS under threshold and previously detected
        elseif vRMS <= sGUI.ArtfctThres && sGUI.sEMG(sGUI.viSide(iStim)).miEMG(6,iData) == -1,
            % Black display
            sGUI.sData(sGUI.viSide(iStim)).ceFile{iData} = regexprep(sGUI.sData(sGUI.viSide(iStim)).ceFile{iData}, '"red"','"black"');
            % Set as good signal in miEMG
            sGUI.sEMG(sGUI.viSide(iStim)).miEMG(6,iData) = 0;
        end
    end
end

if numel(sGUI.viStim) ==1, set(sGUI.sHandles.List_Third,'String',sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile); end

guidata(hObject,sGUI);

end

% == Edit RMS threshold for automatic artifacts detection function
function Edit_ArtfctThreshold_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = str2num(get(hObject,'String'));
if isempty(Value), return; end

% Value must be > 1
if Value <=0,
    sGUI.ArtfctThres = 1;
    set(hObject, 'String', '1');
else
    sGUI.ArtfctThres = Value;
end

guidata(hObject,sGUI);

end

function Edit_ArtfctThreshold_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end


% == Manual artifacts rejection: toggle selected EMGs/conditions between good signal and arifact
function Push_ToggleArtfct_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

for iStim = sGUI.viStim
    % Apply on EMGs or condition ?
    if numel(sGUI.viStim) ==1, viData = sGUI.viEMG; else viData = [1 : numel(sGUI.sData(sGUI.viSide(iStim)).ceData)]; end
    for iData = viData
        % If good signal -> set as artifact
        if sGUI.sEMG(sGUI.viSide(iStim)).miEMG(6,iData) ~= -1,
            sGUI.sData(sGUI.viSide(iStim)).ceFile{iData} = regexprep(sGUI.sData(sGUI.viSide(iStim)).ceFile{iData},'"black"', '"red"');
            sGUI.sEMG(sGUI.viSide(iStim)).miEMG(6,iData) = -1;
            
        % If artifact -> set as good signal
        else
            sGUI.sData(sGUI.viSide(iStim)).ceFile{iData} = regexprep(sGUI.sData(sGUI.viSide(iStim)).ceFile{iData}, '"red"','"black"');
            sGUI.sEMG(sGUI.viSide(iStim)).miEMG(6,iData) = 0;
            
        end
    end
end

if numel(sGUI.viStim) ==1, set(sGUI.sHandles.List_Third,'String',sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile); end

guidata(hObject,sGUI);

end

% == Choose to discard artifacts from further analysis
function Check_ExcludeArtfct_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

sGUI.bExcludeArtfct = get(hObject,'Value');

guidata(hObject,sGUI);

end

% Reverse signals !
function Push_Reverse_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

for iData = 1 : numel(sGUI.sData)
    for iData2 = 1 : numel(sGUI.sData(iData).ceData)
        sGUI.sData(iData).ceData{iData2} = -sGUI.sData(iData).ceData{iData2};
    end
end

% Plotting
sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,0);

guidata(hObject,sGUI);



end
% =================== b. MEP ==============================================

% == Set 1st Param: Sensibility 
function Edit_Sens_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
Value = str2num(get(hObject,'String'));

if isempty(Value), return; end
if Value <=0,
    sGUI.Sens = 0.01;
    set(hObject, 'String', '0.01');
elseif Value > 10,
    sGUI.Sens = 10;
    set(hObject, 'String', '10');
else
    sGUI.Sens = Value;
end

guidata(hObject,sGUI);

% Run MEP Detection
Push_FindMEP_Callback(hObject, eventdata, handles)

end

function Edit_Sens_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% == Set 2nd Param: Min std 
function Edit_StdMin_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
Value = str2num(get(hObject,'String'));

if isempty(Value), return; end
if Value <=0,
    sGUI.StdMin = 1;
    set(hObject, 'String', '1');
else
    sGUI.StdMin = Value;
end

guidata(hObject,sGUI);

% Run MEP Detection
Push_FindMEP_Callback(hObject, eventdata, handles)

end

function Edit_StdMin_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% == Set 3rd Param: Min amp 
function Edit_AmpMin_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
Value = str2num(get(hObject,'String'));

if isempty(Value), return; end
if Value <=0,
    sGUI.AmpMin = 1;
    set(hObject, 'String', '1');
else
    sGUI.AmpMin = Value;
end

guidata(hObject,sGUI);

% Run MEP Detection
Push_FindMEP_Callback(hObject, eventdata, handles)

end
function Edit_AmpMin_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end





function Edit_LatMin_Callback(hObject, eventdata, handles)
sGUI = guidata(hObject);
Value = str2num(get(hObject,'String'));

if isempty(Value), return; end
if Value <=0,
    sGUI.LatMin = 0.015;
    set(hObject, 'String', '15');
else
    sGUI.LatMin = Value*1e-3;
end

guidata(hObject,sGUI);

% Run MEP Detection
Push_FindMEP_Callback(hObject, eventdata, handles)

end

function Edit_LatMin_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end

% == Automatic MEP detection
function Push_FindMEP_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

% If only one condition is selected, apply the function on selected EMGs...
if numel(sGUI.viStim) == 1
    
    % Detection
    [sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG)] = FindMEP(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).vFs(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).vOffset(sGUI.viEMG),sGUI.Sens,sGUI.StdMin,sGUI.AmpMin,sGUI.LatMin);
    
    % Plotting
    sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
        sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,0);
    
    % TL index
    set(sGUI.sHandles.Text_MEP_Q,'String',[' TL = ' num2str(nanmean(sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(5,sGUI.viEMG)))]);

% ... apply it on all EMGs of all selected conditions otherwise
else
    
    hWt = waitbar(0,'Finding MEPs...');
    iWt = 0;
    
    % Detection
    for iStim = sGUI.viStim
       [sGUI.sEMG(sGUI.viSide(iStim)).miEMG] = FindMEP(sGUI.sData(sGUI.viSide(iStim)).ceData,sGUI.sData(sGUI.viSide(iStim)).vFs,sGUI.sData(sGUI.viSide(iStim)).vOffset,sGUI.Sens,sGUI.StdMin,sGUI.AmpMin,sGUI.LatMin,0); 
       iWt = iWt+1;
        waitbar(iWt/numel(sGUI.viStim));
    end
    close(hWt);
end

guidata(hObject,sGUI);

end

% == TL index calculation
function Push_CalcQ_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

% If only one condition is selected, apply the function on selected EMGs...
if numel(sGUI.viStim) == 1
    
    [sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG)] = TLiCalculation(sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),[]);
    
    % TL index
    set(sGUI.sHandles.Text_MEP_Q,'String',[' TL = ' num2str(nanmean(sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(5,sGUI.viEMG)))]);

% ... apply it on all EMGs of all selected conditions otherwise
else
    hWt = waitbar(0,'Calculating TL index...');
    iWt = 0;
    for iStim = sGUI.viStim
        [sGUI.sEMG(sGUI.viSide(iStim)).miEMG] = TLiCalculation(sGUI.sEMG(sGUI.viSide(iStim)).miEMG,sGUI.sData(sGUI.viSide(iStim)).ceData,[]);
        iWt = iWt+1;
        waitbar(iWt/numel(sGUI.viStim));
    end
    close(hWt);
    
end

guidata(hObject,sGUI);

end



% =================== c. CSP ==============================================

% == Set CSP threshold (edit)
function CSPThres_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
Value = str2num(get(hObject,'String'));

if isempty(Value), return; end
% 1 < Value < 200
if Value <=1,
    sGUI.CSPThres = 1e-3; %in µV.ms
    set(hObject, 'String', '1');
elseif Value > 200,
    sGUI.Sens = 200e-3;
    set(hObject, 'String', '200');
else
    sGUI.CSPThres = Value*1e-3;
end

set(sGUI.sHandles.Slide_CSP_Sens,'Value',(sGUI.CSPThres*1e3)/200);
guidata(hObject,sGUI);

% Run CSP Detection
Push_FindCSP_Callback(hObject, eventdata, handles)


end

function CSPThres_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


% == Set CSP threshold (slide)
function Slide_CSP_Sens_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
sGUI.CSPThres = max(1,round(get(hObject,'Value')*200));
set(sGUI.sHandles.CSPThres,'String',num2str(sGUI.CSPThres));
sGUI.CSPThres = sGUI.CSPThres * 1e-3; %in µV.ms

guidata(hObject,sGUI);

% Run CSP Detection
Push_FindCSP_Callback(hObject, eventdata, handles)

end

function Slide_CSP_Sens_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
end

% == Automatic CSP detection
function Push_FindCSP_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

% If only one condition is selected, apply the function on selected EMGs...
if numel(sGUI.viStim) == 1
    
    % Detection
    miEMGTrig = FindCSP(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).vFs(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).vOffset(sGUI.viEMG),sGUI.CSPThres);
    
    if ~isempty(miEMGTrig),
        sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG) = miEMGTrig;
    else
        return;
    end
    
    % Plotting
    sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
        sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,0);

% ... apply it on all EMGs of all selected conditions otherwise
else
    for iStim = sGUI.viStim
       % Detection
       miEMGTrig = FindCSP(sGUI.sData(sGUI.viSide(iStim)).ceData,sGUI.sData(sGUI.viSide(iStim)).vFs,sGUI.sData(sGUI.viSide(iStim)).vOffset,sGUI.CSPThres);
       
       if ~isempty(miEMGTrig),
           sGUI.sEMG(sGUI.viSide(iStim)).miEMG = miEMGTrig;
       else
           return;
       end
    end
    
    
end


guidata(hObject,sGUI);

end




% =========================================================================
% ==================== 2. MARKERS MANAGEMENT PANEL ========================
% =========================================================================


% == Modify existing markers
function Push_ModifyEMGTrig_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

if numel(sGUI.viStim) > 1, 
    warndlg('Not avialable in multi selection mode. Please select only one condition.', 'Warning');
    return;
end

miEMGTrig = ModifyEMGTrig(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),0,sGUI.hFigure);

% Plotting
sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),miEMGTrig,sGUI.hFigure,0);

% User confirmation
cOk = questdlg('OK ?', '', 'Yes', 'No, cancel', 'No, cancel');
if strcmp(cOk, 'Yes'),
    sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG) = miEMGTrig;
else
    sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
        sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,0);
end

guidata(hObject,sGUI);

end


% == Markers suppression
function Push_RemoveEMGTrig_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

if numel(sGUI.viStim) > 1, 
    warndlg('Not avialable in multi selection mode. Please select only one condition.', 'Warning');
    return;
end

miEMGTrig = ModifyEMGTrig(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),1,sGUI.hFigure);

% Plotting
sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),miEMGTrig,sGUI.hFigure,0);

% User confirmation
cOk = questdlg('OK ?', '', 'Yes', 'No, cancel', 'No, cancel');
if strcmp(cOk, 'Yes'),
    sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG) = miEMGTrig;
else
    sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
        sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,0);
end

guidata(hObject,sGUI);

end


% == Add manually Peak_1
function Push_Add_EMG_Min_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

if numel(sGUI.viStim) > 1, 
    warndlg('Not avialable in multi selection mode. Please select only one condition.', 'Warning');
    return;
end

miEMGTrig = AddEMGTrig(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,'Min');
if isempty(miEMGTrig), return; end

% Plotting
sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),miEMGTrig,sGUI.hFigure,0);

% User confirmation
cOk = questdlg('OK ?', '', 'Yes', 'No, cancel', 'No, cancel');
if strcmp(cOk, 'Yes'),
    sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG) = miEMGTrig;
else
    sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
        sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,0);
end

guidata(hObject,sGUI);

end


% == Add manually Peak_2
function Push_Add_EMG_Max_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

if numel(sGUI.viStim) > 1, 
    warndlg('Not avialable in multi selection mode. Please select only one condition.', 'Warning');
    return;
end

miEMGTrig = AddEMGTrig(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,'Max');
if isempty(miEMGTrig), return; end

% Plotting
sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),miEMGTrig,sGUI.hFigure,0);

% User confirmation
cOk = questdlg('OK ?', '', 'Yes', 'No, cancel', 'No, cancel');
if strcmp(cOk, 'Yes'),
    sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG) = miEMGTrig;
else
    sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
        sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,0);
end

guidata(hObject,sGUI);

end

% == Add manually start
function Push_Add_EMG_Start_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

if numel(sGUI.viStim) > 1, 
    warndlg('Not avialable in multi selection mode. Please select only one condition.', 'Warning');
    return;
end

miEMGTrig = AddEMGTrig(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,'Start');
if isempty(miEMGTrig), return; end

% Plotting
sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),miEMGTrig,sGUI.hFigure,0);

% User confirmation
cOk = questdlg('OK ?', '', 'Yes', 'No, cancel', 'No, cancel');
if strcmp(cOk, 'Yes'),
    sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG) = miEMGTrig;
else
    sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
        sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,0);
end

guidata(hObject,sGUI);

end

% == Add manually end
function Push_Add_EMG_End_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

if numel(sGUI.viStim) > 1, 
    warndlg('Not avialable in multi selection mode. Please select only one condition.', 'Warning');
    return;
end

miEMGTrig = AddEMGTrig(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,'End');
if isempty(miEMGTrig), return; end

% Plotting
sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
    sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),miEMGTrig,sGUI.hFigure,0);

% User confirmation
cOk = questdlg('OK ?', '', 'Yes', 'No, cancel', 'No, cancel');
if strcmp(cOk, 'Yes'),
    sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG) = miEMGTrig;
else
    sGUI.hFigure = PlotEMG(sGUI.sData(sGUI.viSide(sGUI.viStim)).ceTime(sGUI.viEMG),sGUI.sData(sGUI.viSide(sGUI.viStim)).ceData(sGUI.viEMG),...
        sGUI.sData(sGUI.viSide(sGUI.viStim)).ceFile(sGUI.viEMG),sGUI.sEMG(sGUI.viSide(sGUI.viStim)).miEMG(:,sGUI.viEMG),sGUI.hFigure,0);
end

guidata(hObject,sGUI);

end

% == Save marker file (.mat file)
function Push_SaveFile_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
global MEP_TOOLBOX_VER

cOk = questdlg('Save file ?', '', 'Yes', 'No, cancel', 'No, cancel');

if strcmp(cOk, 'Yes'),
    if ~sGUI.bGroup
        sEMG = sGUI.sEMG;
        ceSide = sGUI.ceSide;
        MEP_Toolbox_Ver = MEP_TOOLBOX_VER;
        if isfield(sGUI.sData,'cEMGChan')
            save([sGUI.cPath filesep sGUI.cPath(max(strfind(sGUI.cPath,filesep))+1:end) '_EMG_trig_' sGUI.sData.cEMGChan '.mat'],'sEMG','ceSide','MEP_Toolbox_Ver');
        else
            save([sGUI.cPath filesep sGUI.cPath(max(strfind(sGUI.cPath,filesep))+1:end) '_EMG_trig.mat'],'sEMG','ceSide','MEP_Toolbox_Ver');
        end
    else
        ceData = sGUI.ceData;
        ceDataBckp = sGUI.ceDataBckp;
        ceEMG = sGUI.ceEMG;
        cePatient = sGUI.cePatient;
        ceCond = sGUI.ceCond;
        ceCondBckp = sGUI.ceCondBckp;
        
        MEP_Toolbox_Ver = MEP_TOOLBOX_VER;
        save([sGUI.cPath filesep sGUI.cPath(max(strfind(sGUI.cPath,filesep))+1:end) '_GrpStudy.mat'],'ceData','ceDataBckp','ceEMG','cePatient','ceCond','ceCondBckp','MEP_Toolbox_Ver');
    end
end

end


% == Erase previous marker file
function Push_EraseFile_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
global MEP_TOOLBOX_VER

cOk = questdlg('Erase EMG ?', '', 'Yes', 'No, cancel', 'No, cancel');

if strcmp(cOk, 'Yes'),
    if isunix
        cCmd = ['!rm ' sGUI.cPath filesep '*.mat'];
        eval(cCmd);
    elseif ispc
        cCmd = ['!del ' sGUI.cPath filesep '*.mat'];
        eval(cCmd);
    end
end

end



% =========================================================================
% ==================== 3. RESULTS PANEL ===================================
% =========================================================================

% =================== a. EXPORT ===========================================

% == Spreadsheet export
function Push_exportxls_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

% Spreadsheet dimension: conditions * EMGs * features
NbSheet = 7;
NbCond = numel(sGUI.sData);
for iCond = 1 : NbCond, vNbEMG(iCond) = numel(sGUI.sData(iCond).ceData); end
NbEMG = max(vNbEMG);

ceXls = cell(NbCond*2+1,NbEMG+1,NbSheet); 

ceXls{1,1,1} = 'AMPLITUDE (microV)';
ceXls{1,1,2} = 'RMS (microV)';
ceXls{1,1,3} = 'LATENCY (ms)';
ceXls{1,1,4} = 'PEAK LATENCY (ms)';
ceXls{1,1,5} = 'INTERVAL (ms)';
ceXls{1,1,6} = 'TL INDEX (0-100)';
ceXls{1,1,7} = 'CHRONO ORDER';

% Header
for iSheet = 1 : NbSheet
    %TO DO !!!
    for iCond = 1 : NbCond
        for iLign = 2 :  numel(sGUI.sData(iCond).ceData)+1,
            Start = strfind(sGUI.sData(iCond).ceFile{iLign-1},'>');
            ceXls{iLign,2*(iCond-1)+2,iSheet} = sGUI.sData(iCond).ceFile{iLign-1}(Start(end)+1:end);
            %         ceXls{2,iLign,iSheet} = ['EMG #' num2str(iLign-1)]; %old - no inf
        end
    end
end

for iCond = 1 : NbCond
    % Condition name
    for iSheet = 1 : NbSheet
        ceXls{1,2*(iCond-1)+3,iSheet} = sGUI.sData(iCond).cName;
        ceXls{1,2*(iCond-1)+2,iSheet} = sGUI.sData(iCond).cName;
    end
    % Amplitude
    vMEPAmp = MEPAmpCalculation(sGUI.sData(iCond).ceData,sGUI.sEMG(iCond).miEMG,[],0,'Zero');
    % RMS
    vMEPrms = MEPAmpCalculation(sGUI.sData(iCond).ceData,sGUI.sEMG(iCond).miEMG,[],1,'Zero');
    % Latency
    vLatency = 1000.*(sGUI.sEMG(iCond).miEMG(3,:)-sGUI.sData(iCond).vOffset) ./ sGUI.sData(iCond).vFs;
    % Peak latency
    vPkLatency = 1000.*(min(sGUI.sEMG(iCond).miEMG(1:2,:))-sGUI.sData(iCond).vOffset) ./ sGUI.sData(iCond).vFs;
    % Time interval
    vDuration =  sGUI.sEMG(iCond).miEMG(4,:)-sGUI.sEMG(iCond).miEMG(3,:);
    vDuration = 1000.*vDuration ./ sGUI.sData(iCond).vFs;
    
    % If artifacts to be discarded -> set as NaN
    if sGUI.bExcludeArtfct,
        viArtfct = find((sGUI.sEMG(iCond).miEMG(6,:))==-1);
        vMEPAmp(viArtfct) = -999;
        vMEPrms(viArtfct) = -999;
        vLatency(viArtfct) = -999;
        vPkLatency(viArtfct) = -999;
        vDuration(viArtfct) = -999;
        sGUI.sEMG(iCond).miEMG(5,viArtfct) = -999;
    end
    
    % Filling spreadsheet
    for iLign = 2 : 2+numel(vMEPAmp)-1, ceXls{iLign,2*(iCond-1)+3,1} = vMEPAmp(iLign-1); end
    for iLign = 2 : 2+numel(vMEPAmp)-1, ceXls{iLign,2*(iCond-1)+3,2} = vMEPrms(iLign-1); end
    for iLign = 2 : 2+numel(vMEPAmp)-1, ceXls{iLign,2*(iCond-1)+3,3} = vLatency(iLign-1); end
    for iLign = 2 : 2+numel(vMEPAmp)-1, ceXls{iLign,2*(iCond-1)+3,4} = vPkLatency(iLign-1); end
    for iLign = 2 : 2+numel(vMEPAmp)-1, ceXls{iLign,2*(iCond-1)+3,5} = vDuration(iLign-1); end
    for iLign = 2 : 2+numel(vMEPAmp)-1, ceXls{iLign,2*(iCond-1)+3,6} = sGUI.sEMG(iCond).miEMG(5,iLign-1); end
    for iLign = 2 : 2+numel(vMEPAmp)-1, ceXls{iLign,2*(iCond-1)+3,7} = sGUI.sData(iCond).vOrder(iLign-1); end
end

% Save as .ods or .xls, depending on explorating system
if sGUI.bUnix
    [cFile, cPath] = uiputfile('*.ods', 'Save Ods file as');
    if cFile~=0
        for iSheet = 1 : NbSheet
            myxlswrite([cPath cFile(1:end-4)],ceXls(:,:,iSheet),iSheet,'A1'); %myxls rajoute .ods par defaut
        end
    end
else
    
    warning off MATLAB:xlswrite:AddSheet
    [cFile, cPath] = uiputfile('*.xls', 'Save Excel file as');
    if cFile~=0
        for iSheet = 1 : NbSheet
            xlswrite([cPath cFile],ceXls(:,:,iSheet),iSheet);
        end
    end
end

end

% == Figure export
function Push_SaveFig_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Fig2 = figure;
copyobj(handles.sHandles.Axes, Fig2);
 
set(get(Fig2,'Children'),'Units','normalized');
set(get(Fig2,'Children'),'Position', [0.1 0.1 0.85 0.85]);
 
% Display legend if needed
if numel(get(handles.sHandles.figure1,'Children')) > 1
    % warning latex...
    warning off MATLAB:gui:latexsup:BadTeXString
    legend show
end

end



% ======================= b. FEATURES DISPLAY =============================

% == Choose which feature to display : Latency
function Radio_FtLat_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_FtLat,'value');
if Value,
    set(sGUI.sHandles.Radio_FtDur,'Value',~Value);
    set(sGUI.sHandles.Radio_FtQ,'Value',~Value);
    set(sGUI.sHandles.Radio_FtAmp,'Value',~Value);
    set(sGUI.sHandles.Radio_FtRMS,'Value',~Value);
    sGUI.cFeatPlot = 'Lat';
end

guidata(hObject,sGUI);

end

% == Time interval / duration
function Radio_FtDur_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_FtDur,'value');
if Value == 1,
    set(sGUI.sHandles.Radio_FtLat,'Value',~Value);
    set(sGUI.sHandles.Radio_FtQ,'Value',~Value);
    set(sGUI.sHandles.Radio_FtAmp,'Value',~Value);
    set(sGUI.sHandles.Radio_FtRMS,'Value',~Value);
    sGUI.cFeatPlot = 'Dur';
end

guidata(hObject,sGUI);

end

% == Peak 2 peak amplitude
function Radio_FtAmp_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_FtAmp,'value');
if Value == 1,
    set(sGUI.sHandles.Radio_FtDur,'Value',~Value);
    set(sGUI.sHandles.Radio_FtQ,'Value',~Value);
    set(sGUI.sHandles.Radio_FtLat,'Value',~Value);
    set(sGUI.sHandles.Radio_FtRMS,'Value',~Value);
    sGUI.cFeatPlot = 'Amp';
end

guidata(hObject,sGUI);

end

% == TL index
function Radio_FtQ_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_FtQ,'value');
if Value == 1,
    set(sGUI.sHandles.Radio_FtDur,'Value',~Value);
    set(sGUI.sHandles.Radio_FtLat,'Value',~Value);
    set(sGUI.sHandles.Radio_FtAmp,'Value',~Value);
    set(sGUI.sHandles.Radio_FtRMS,'Value',~Value);
    sGUI.cFeatPlot = 'TLi';
end

guidata(hObject,sGUI);

end

% == RMS
function Radio_FtRMS_Callback(hObject, eventdata, handles)
sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_FtRMS,'value');
if Value == 1,
    set(sGUI.sHandles.Radio_FtDur,'Value',~Value);
    set(sGUI.sHandles.Radio_FtLat,'Value',~Value);
    set(sGUI.sHandles.Radio_FtAmp,'Value',~Value);
    set(sGUI.sHandles.Radio_FtQ,'Value',~Value);
    sGUI.cFeatPlot = 'RMS';
end

guidata(hObject,sGUI);

end


% == Selected feature display
function Push_PlotFeat_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

% Feature means calculation
vMean = [];
vStd = [];
for iStim = sGUI.viStim

    switch sGUI.cFeatPlot
        case 'Amp'
            vData = MEPAmpCalculation(sGUI.sData(sGUI.viSide(iStim)).ceData,sGUI.sEMG(sGUI.viSide(iStim)).miEMG,[],0,'Zero');
            cTitle = 'Mean amplitude (µV)';
        case 'RMS'
            vData = MEPAmpCalculation(sGUI.sData(sGUI.viSide(iStim)).ceData,sGUI.sEMG(sGUI.viSide(iStim)).miEMG,[],1,'Zero');
            cTitle = 'RMS (µV)';
        case 'TLi'
            vData = sGUI.sEMG(sGUI.viSide(iStim)).miEMG(5,:);
            cTitle = 'Mean TL index (%)';
        case 'Lat'
            vData = 1000*(sGUI.sEMG(sGUI.viSide(iStim)).miEMG(3,:)-sGUI.sData(sGUI.viSide(iStim)).vOffset) ./ sGUI.sData(sGUI.viSide(iStim)).vFs; %en ms
            cTitle = 'Mean latency (ms)';
        case 'Dur'
            vTime =  sGUI.sEMG(sGUI.viSide(iStim)).miEMG(4,:)-sGUI.sEMG(sGUI.viSide(iStim)).miEMG(3,:);
            vData = 1000.*vTime ./ sGUI.sData(sGUI.viSide(iStim)).vFs; % en ms
            cTitle = 'Mean duration (ms)';
    end

    % If artifact to be discarded => value set to NaN
    if sGUI.bExcludeArtfct,
        viArtfct = find((sGUI.sEMG(sGUI.viSide(iStim)).miEMG(6,:))==-1);
        vData(viArtfct) = NaN;
    end
    
    vMean(end+1) = nanmean(vData);
    vStd(end+1) = nanstd(vData);
    
    ceAllData{iStim} = vData;
    
end

% Plotting
hFigure = sGUI.sHandles.figure1;
cla(sGUI.sHandles.Axes,'reset');
sGUI.hFigure = PlotBar(vMean,vStd,'b',{sGUI.sData(sGUI.viSide(sGUI.viStim)).cName},[],hFigure,[],cTitle); hold on;
set(sGUI.sHandles.Text_MEP_Q, 'String', cTitle);


% 2 by 2 statistics
miStimComb = combnk(sGUI.viStim,2);
disp(['Statistical results for ' sGUI.cFeatPlot ':']);
bRes = 0;
for iStimComb = 1 : size(miStimComb,1)
    [h,p] = ttest2(ceAllData{miStimComb(iStimComb,1)},ceAllData{miStimComb(iStimComb,2)});
    if h>0, 
        disp([ sGUI.sData(sGUI.viSide(miStimComb(iStimComb,1))).cName ' is sign. different from ' ...
           sGUI.sData(sGUI.viSide(miStimComb(iStimComb,2))).cName ', p = ' num2str(p)]); 
       bRes = 1;
    end
end

if ~bRes, disp('None'); end

guidata(hObject,sGUI);

end



% ======================= c. SR CURVE =====================================

function Push_SRCurve_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

if numel(sGUI.viStim) <= 1, 
    warndlg('Not avialable in single selection mode. Please select conditions to plot.', 'Warning');
    return;
end

% Amplitude means calculation
ceMEPAmp = {};
vMEPMean = [];
vMEPStd = [];
for iStim = sGUI.viStim

    ceMEPAmp{end+1} = MEPAmpCalculation(sGUI.sData(sGUI.viSide(iStim)).ceData,sGUI.sEMG(sGUI.viSide(iStim)).miEMG,[],0,'Zero');
    vMEPMean(end+1) = nanmean(ceMEPAmp{end});
    vMEPStd(end+1) = nanstd(ceMEPAmp{end});

end

hFigure = sGUI.sHandles.figure1;

cla(sGUI.sHandles.Axes,'reset');

% Loading text file containing stimulation intensities
if exist([sGUI.cPath filesep 'SR_x.txt']) == 2
    vXval = load([sGUI.cPath filesep 'SR_x.txt'])';
else
    vXval = [1:numel(vMEPMean)];
end

% Single point plotting
for iVal = 1 : numel(vXval), 
    plot(vXval(iVal),ceMEPAmp{iVal}','b*','MarkerSize',2); hold on; 
    plot(vXval(iVal),vMEPMean(iVal)','o','MarkerEdgeColor','k','MarkerFaceColor','m','MarkerSize',4);
end
set(gca,'XTick',sort(vXval));
set(gca,'XTickLabel',{sort(vXval)});
xlabel('Stimulation power (%)','FontSize',10);
ylabel('Amplitude (microV)','FontSize',10);

% 4 parameters sigmoid function
fhSigmo = @(p,x) abs(p(1)) + p(4)./(1+exp(-p(2).*(x-p(3))));

% Parameters estimation (non linear)
vBO = [nanmin(vMEPMean) 1 round(max(vXval)/2) max(vMEPMean)];
vB = nlinfit(vXval, vMEPMean, fhSigmo, vBO);

% Sigmoid plotting 
vX = [10 :  1 : 100];
plot(vX, fhSigmo(vB,vX), 'r','LineWidth',2);

set(sGUI.sHandles.Text_MEP_Q, 'String', '');

guidata(hObject,sGUI);

end



% ======================= c. GRID / MAPPING ===============================

function Push_2DGrid_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

if numel(sGUI.viStim) == 1, 
    warndlg('Not avialable in single selection mode. Please select conditions to plot.', 'Warning');
    return;
end

% X and Y grid size
cePrompt={'Grid size (X)','Grid size(Y)','Interpolation factor (0 = raw data)'};
cName='Plot parameters';
Numlines=1;
ceDefaultanswer={'','','7'};

ceAnswer=inputdlg(cePrompt,cName,Numlines,ceDefaultanswer);
Size_X = str2num(ceAnswer{1});
Size_Y = str2num(ceAnswer{2});
NbInterp = str2num(ceAnswer{3});

% Feature means calculation
mMEPMean = zeros(Size_Y,Size_X);
mMEPStd = zeros(Size_Y,Size_X);
iGrid = 1;

for iStim = sGUI.viStim
    
    switch sGUI.cFeatPlot
        case 'Amp'
            vData = MEPAmpCalculation(sGUI.sData(sGUI.viSide(iStim)).ceData,sGUI.sEMG(sGUI.viSide(iStim)).miEMG,[],0,'Zero');
            cTitle = 'Mean amplitude (�V)';
        case 'RMS'
            vData = MEPAmpCalculation(sGUI.sData(sGUI.viSide(iStim)).ceData,sGUI.sEMG(sGUI.viSide(iStim)).miEMG,[],1,'Zero');
            cTitle = 'RMS (�V)';
        case 'TLi'
            vData = sGUI.sEMG(sGUI.viSide(iStim)).miEMG(5,:);
            cTitle = 'Mean TL index (%)';
        case 'Lat'
            vData = 1000*(sGUI.sEMG(sGUI.viSide(iStim)).miEMG(3,:)-sGUI.sData(sGUI.viSide(iStim)).vOffset) ./ sGUI.sData(sGUI.viSide(iStim)).vFs; %en ms
            cTitle = 'Mean latency (ms)';
        case 'Dur'
            vTime =  sGUI.sEMG(sGUI.viSide(iStim)).miEMG(4,:)-sGUI.sEMG(sGUI.viSide(iStim)).miEMG(3,:);
            vData = 1000.*vTime ./ sGUI.sData(sGUI.viSide(iStim)).vFs; % en ms
            cTitle = 'Mean duration (ms)';
    end

    % If artifacts to discard -> set value to NaN
    if sGUI.bExcludeArtfct,
        viArtfct = find((sGUI.sEMG(sGUI.viSide(iStim)).miEMG(6,:))==-1);
        vData(viArtfct) = NaN;
    end
    
    mMEPMean(Size_Y-floor((iGrid-1)/Size_X),Size_X-mod(iGrid-1,Size_X)) = nanmean(vData);
    mMEPStd(Size_Y-floor((iGrid-1)/Size_X),Size_X-mod(iGrid-1,Size_X)) = nanstd(vData);
    ceHS{Size_Y-floor((iGrid-1)/Size_X),Size_X-mod(iGrid-1,Size_X)}=vData;
    iGrid = iGrid+1;

end
% Grid is filled in a zigzag way
mMEPMean([Size_Y-1:-2:1],:) = fliplr(mMEPMean([Size_Y-1:-2:1],:));
mMEPStd([Size_Y-1:-2:1],:) = fliplr(mMEPStd([Size_Y-1:-2:1],:));
mMEPMean = fliplr(mMEPMean');
mMEPStd = fliplr(mMEPStd');
ceHS([Size_Y-1:-2:1],:) = fliplr(ceHS([Size_Y-1:-2:1],:));
ceHS = fliplr(ceHS');

hFigure = sGUI.sHandles.figure1;

cla(sGUI.sHandles.Axes,'reset');

% Plotting map
mIm = interp2(mMEPMean,NbInterp,'cubic');
figure(hFigure), imagesc(mIm), axis equal;

% Plotting stimulation sites
hold on
plot(repmat([1:(size(mIm,2)-1)/(size(mMEPMean,2)-1):size(mIm,2)],size(mMEPMean,1),1),repmat([1:(size(mIm,1)-1)/(size(mMEPMean,1)-1):size(mIm,1)]',1,size(mMEPMean,2)),'ok','MarkerSize',5)

% Plotting Center of gravity
Xcog = sum([1:size(mIm,2)].*mean(mIm,1))/(sum(mean(mIm,1)));
Ycog = sum([1:size(mIm,1)].*mean(mIm,2)')/(sum(mean(mIm,2)));
plot(Xcog,Ycog,'kx','MarkerSize',30,'LineWidth',10);
hold off

% Axes and title
set(sGUI.sHandles.Text_MEP_Q, 'String', ['Mapping: ' sGUI.cFeatPlot]);

set(gca,'XTickLabel','');
set(gca,'YTickLabel','');
xlabel('X','FontSize',10);
ylabel('Y','FontSize',10);
xlim([1 size(mIm,2)]);
ylim([1 size(mIm,1)]);
colorbar;



guidata(hObject,sGUI);

end



% =========================================================================
% ==================== GROUP STUDY PANEL ==================================
% =========================================================================

% == Baseline definition: user set a condition as baseline
function Push_SetBsl_Callback(hObject, eventdata, handles)
    
sGUI = guidata(hObject);

iBslCond = get(sGUI.sHandles.List_Second,'value');

if numel(iBslCond) > 1, 
    errordlg('Please select only one condition for baseline. If more than one condition must be defined as baseline, please merge them before.','Error');
    return;
end

% User confirmation
cQst = questdlg(['Set selected conditions as baseline ?'], '', 'Yes', 'No, cancel', 'No, cancel');
if strcmp(cQst, 'Yes'),
    
    % Baseline highlighted in red
    if ~isempty(sGUI.iBslCond), sGUI.ceCond{1,sGUI.iBslCond} = regexprep(sGUI.ceCond{1,sGUI.iBslCond}, '"red"','"black"'); end
    
    sGUI.ceCond{1,iBslCond} = regexprep(sGUI.ceCond{1,iBslCond}, '"black"','"red"');
    
    set(sGUI.sHandles.List_Third,'String',{});
    set(sGUI.sHandles.List_Third,'Value',1);
    set(sGUI.sHandles.List_Second, 'String', sGUI.ceCond(1,:));
    
    sGUI.iBslCond = iBslCond;
    
else

end

guidata(hObject,sGUI);

end

% == Remove baseline
function Push_NoBsl_Callback(hObject, eventdata, handles)
sGUI = guidata(hObject);

iBslCond = [];

if ~isempty(sGUI.iBslCond), sGUI.ceCond{1,sGUI.iBslCond} = regexprep(sGUI.ceCond{1,sGUI.iBslCond}, '"red"','"black"'); end

set(sGUI.sHandles.List_Third,'String',{});
set(sGUI.sHandles.List_Third,'Value',1);
set(sGUI.sHandles.List_Second, 'String', sGUI.ceCond(1,:));

sGUI.iBslCond = iBslCond;

guidata(hObject,sGUI);

end

% == Save group study file (.mat file)
function Push_SaveGrp_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);
global MEP_TOOLBOX_VER

cOk = questdlg('Save file ?', '', 'Yes', 'No, cancel', 'No, cancel');

if strcmp(cOk, 'Yes'),
    if ~sGUI.bGroup
        sEMG = sGUI.sEMG;
        ceSide = sGUI.ceSide;
        MEP_Toolbox_Ver = MEP_TOOLBOX_VER;
        save([sGUI.cPath filesep sGUI.cPath(max(strfind(sGUI.cPath,filesep))+1:end) '_EMG_trig.mat'],'sEMG','ceSide','MEP_Toolbox_Ver');
    else
        ceData = sGUI.ceData;
        ceDataBckp = sGUI.ceDataBckp;
        ceEMG = sGUI.ceEMG;
        cePatient = sGUI.cePatient;
        ceCond = sGUI.ceCond;
        ceCondBckp = sGUI.ceCondBckp;
        
        MEP_Toolbox_Ver = MEP_TOOLBOX_VER;
        save([sGUI.cPath filesep sGUI.cPath(max(strfind(sGUI.cPath,filesep))+1:end) '_GrpStudy.mat'],'ceData','ceDataBckp','ceEMG','cePatient','ceCond','ceCondBckp','MEP_Toolbox_Ver');
    end
end

end


% == Group selected conditions
function Push_GroupCond_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

viCond = get(sGUI.sHandles.List_Second,'value');

cQst = questdlg(['Merge selected conditions ?'], '', 'Yes', 'No, cancel', 'No, cancel');
if strcmp(cQst, 'Yes'),

    ceNewName = inputdlg('Condition Name','New condition name',1,{'NewCond'});
    [sGUI.ceData sGUI.ceCond] = RearrangeCond(sGUI.ceData, sGUI.ceCond, viCond, ceNewName);
    set(sGUI.sHandles.List_Second, 'String', sGUI.ceCond(1,:));
    set(sGUI.sHandles.List_Second,'Value',[]);
    set(sGUI.sHandles.List_Third,'String',{});
    set(sGUI.sHandles.List_Third,'Value',1);
    
else

end

guidata(hObject,sGUI);

end

% == Remove selected conditions
function Push_RmCond_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

viCond = get(sGUI.sHandles.List_Second,'value');

cQst = questdlg(['Remove selected conditions ?'], '', 'Yes', 'No, cancel', 'No, cancel');
if strcmp(cQst, 'Yes'),

    sGUI.ceCond(:,viCond) = [];
    set(sGUI.sHandles.List_Second, 'String', sGUI.ceCond(1,:));
    set(sGUI.sHandles.List_Second,'Value',[]);
    set(sGUI.sHandles.List_Third,'String',{});
    set(sGUI.sHandles.List_Third,'Value',1);
    
else

end

guidata(hObject,sGUI);

end

% == Feature selection : Amp
function Radio_GpAmp_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_GpAmp,'value');
if Value,
    sGUI.cCalcMeasure = 'Amp';
    set(sGUI.sHandles.Radio_GpQ,'Value',~Value);
    set(sGUI.sHandles.Radio_GpRMS,'Value',~Value);
    set(sGUI.sHandles.Radio_GpLat,'Value',~Value);
    set(sGUI.sHandles.Radio_GpDur,'Value',~Value);
end

guidata(hObject,sGUI);


end

% == TL index
function Radio_GpQ_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_GpQ,'value');
if Value,
    sGUI.cCalcMeasure = 'TLi';
    set(sGUI.sHandles.Radio_GpAmp,'Value',~Value);
    set(sGUI.sHandles.Radio_GpRMS,'Value',~Value);
    set(sGUI.sHandles.Radio_GpLat,'Value',~Value);
    set(sGUI.sHandles.Radio_GpDur,'Value',~Value);
end

guidata(hObject,sGUI);

end

% == RMS
function Radio_GpRMS_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_GpRMS,'value');
if Value,
    sGUI.cCalcMeasure = 'RMS';
    set(sGUI.sHandles.Radio_GpAmp,'Value',~Value);
    set(sGUI.sHandles.Radio_GpQ,'Value',~Value);
    set(sGUI.sHandles.Radio_GpLat,'Value',~Value);
    set(sGUI.sHandles.Radio_GpDur,'Value',~Value);
end

guidata(hObject,sGUI);

end

% == Time interval / duration
function Radio_GpDur_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_GpDur,'value');
if Value,
    sGUI.cCalcMeasure = 'Dur';
    set(sGUI.sHandles.Radio_GpAmp,'Value',~Value);
    set(sGUI.sHandles.Radio_GpQ,'Value',~Value);
    set(sGUI.sHandles.Radio_GpLat,'Value',~Value);
    set(sGUI.sHandles.Radio_GpRMS,'Value',~Value);
end

guidata(hObject,sGUI);
end

% == Latency
function Radio_GpLat_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_GpLat,'value');
if Value,
    sGUI.cCalcMeasure = 'Lat';
    set(sGUI.sHandles.Radio_GpAmp,'Value',~Value);
    set(sGUI.sHandles.Radio_GpQ,'Value',~Value);
    set(sGUI.sHandles.Radio_GpDur,'Value',~Value);
    set(sGUI.sHandles.Radio_GpRMS,'Value',~Value);
end

guidata(hObject,sGUI);
end

% == Mean baseline calculated with all EMGs
function Radio_Bsl_All_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_Bsl_All,'value');
set(sGUI.sHandles.Radio_Bsl_5bests,'Value',0);
set(sGUI.sHandles.Radio_Bsl_Med,'Value',0);

if Value == 1
    sGUI.cCalcMethod = 'All';
else
    sGUI.cCalcMethod = 'Raw';
end

guidata(hObject,sGUI);

end

% == Mean baseline calculated with the 5 best EMGs
function Radio_Bsl_5bests_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_Bsl_5bests,'value');
set(sGUI.sHandles.Radio_Bsl_All,'Value',0);
set(sGUI.sHandles.Radio_Bsl_Med,'Value',0);

if Value == 1
    sGUI.cCalcMethod = '5Bests';
else
    sGUI.cCalcMethod = 'Raw';
end

guidata(hObject,sGUI);
end

% == Baseline calculated with the median
function Radio_Bsl_Med_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

Value = get(sGUI.sHandles.Radio_Bsl_Med,'value');
set(sGUI.sHandles.Radio_Bsl_5bests,'Value',0);
set(sGUI.sHandles.Radio_Bsl_All,'Value',0);

if Value == 1
    sGUI.cCalcMethod = 'Med';
else
    sGUI.cCalcMethod = 'Raw';
end

guidata(hObject,sGUI);
end

% == Choose to display the baseline
function Check_ShowBsl_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

sGUI.bShowBsl = get(sGUI.sHandles.Check_ShowBsl,'value');

guidata(hObject,sGUI);

if isempty(get(sGUI.sHandles.List_Third,'String')),
    Push_GroupStat_Callback(sGUI.sHandles.Push_GroupStat, eventdata, handles);
else    
    List_Third_Callback(sGUI.sHandles.List_Third, eventdata, handles);
end

end

% == Compute individual statistics
function Push_IndStat_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

sGUI.sIndStat = ComputeIndStat(sGUI.ceData,sGUI.ceEMG,sGUI.ceCond,sGUI.iBslCond,sGUI.cCalcMeasure,sGUI.cCalcMethod);

set(sGUI.sHandles.List_Third, 'String', sGUI.cePatient);
set(sGUI.sHandles.List_Third,'Value',1);

guidata(hObject,sGUI);


end

% == Compute group statistics
function Push_GroupStat_Callback(hObject, eventdata, handles)

sGUI = guidata(hObject);

% Compute stats
sGUI.mGroupStat = ComputeGroupStat(sGUI.sIndStat);

set(sGUI.sHandles.List_Third, 'String', {});
set(sGUI.sHandles.List_Third,'Value',1);

% Plot group results
hFigure = sGUI.sHandles.figure1;

cla(sGUI.sHandles.Axes,'reset');
switch sGUI.cCalcMeasure
    case 'Amp'
        cYlabel = 'Amplitude (µV)';
    case 'TLi'
        cYlabel = 'TL index ( %)';
    case 'RMS'
        cYlabel = 'RMS (µV)';
    case 'Lat'
        cYlabel = 'Latency (ms)';
    case 'Dur'
        cYlabel = 'Duration (ms)';
end
if ~isempty(sGUI.iBslCond), cYlabel(end-3:end+6) = '(% of bsl)'; end
if sGUI.bShowBsl, viCond = [1:length(sGUI.ceCond)]; else viCond = setxor([1:length(sGUI.ceCond)],sGUI.iBslCond); end
sGUI.hFigure = PlotBar(sGUI.mGroupStat(1,viCond),sGUI.mGroupStat(2,viCond),...
    'b',sGUI.ceCond(1,viCond),[],hFigure,[],cYlabel);

guidata(hObject,sGUI);

end


function mGroupStat = ComputeGroupStat(sIndStat)

for iCond = 1:length(sIndStat(1).mMeanStd)
    for iData = 1 : numel(sIndStat)
        vStat(iData) = sIndStat(iData).mMeanStd(1,iCond);
    end
    mGroupStat(1,iCond) = nanmean(vStat);
    mGroupStat(2,iCond) = nanstd(vStat);
    mGroupData(:,iCond) = vStat;
end

% TO DO : group paired t-tests

% ttest(mGroupData(:,1),mGroupData(:,2))
% ttest(mGroupData(:,1),mGroupData(:,3))
% ttest(mGroupData(:,1),mGroupData(:,4))


end






% =========================================================================
% ==================== EXTERNAL FUNCTIONS =================================
% =========================================================================

% == Plotting function
function [hFigure] = PlotEMG(ceTime,ceData,ceName,miEMG,hFigure,bYreset,vFs)

if hFigure == -1,
    hFigure = figure;
else
    figure(hFigure);
end
if isempty(miEMG), miEMG = zeros(2,numel(ceData)); end

mColor = colormap('jet');

YLim = get(gca,'YLim');
XLim = get(gca,'XLim');
cla reset

% Plot each data, color defined using jet colormap
for iPlot = 1 : numel(ceData)
    
    
        

    
    
    hLine = plot(ceTime{iPlot},ceData{iPlot},'LineWidth',1);
    set(hLine,'Color',mColor(floor((iPlot-1)*64/numel(ceData))+1,:));
    hold on;
    if miEMG(1,iPlot)~=0,
        hLine = plot(ceTime{iPlot}(miEMG(1,iPlot)),ceData{iPlot}(miEMG(1,iPlot)),'o','MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',5);
        set(hLine,'HandleVisibility','Off');
    end
    if miEMG(2,iPlot)~=0,
        hLine = plot(ceTime{iPlot}(miEMG(2,iPlot)),ceData{iPlot}(miEMG(2,iPlot)),'o','MarkerEdgeColor','k','MarkerFaceColor','b','MarkerSize',5);
        set(hLine,'HandleVisibility','Off');
    end
    try % backward compatibility (v 0.2)
        if miEMG(3,iPlot)~=0,
            hLine = plot(ceTime{iPlot}(miEMG(3,iPlot)),0,'o','MarkerEdgeColor','k','MarkerFaceColor','g','MarkerSize',5);
            set(hLine,'HandleVisibility','Off');
        end
        if miEMG(4,iPlot)~=0,
            hLine = plot(ceTime{iPlot}(miEMG(4,iPlot)),0,'o','MarkerEdgeColor','k','MarkerFaceColor','g','MarkerSize',5);
            set(hLine,'HandleVisibility','Off');
        end
    catch
    end
end

% Horizontal bar ("0")
hLine = plot([min([ceTime{:}]) max([ceTime{:}])],[0 0],'k--');
set(hLine,'HandleVisibility','Off');

% Vertical bar (Stim start)
YLim_tmp = get(gca,'YLim');
XLim_tmp = get(gca,'XLim');
hLine = plot([0 0],[-1e15 +1e15],'k--');

set(hLine,'HandleVisibility','Off');
set(gca,'YLim',YLim_tmp);
set(gca,'XLim',XLim_tmp);
set(gca,'FontSize',8);
xlabel('Time (s)','FontSize',10);
ylabel('Amplitude (microV)','FontSize',10);

% Removing html prefix for legend entries
for iName = 1 : numel(ceName)
    Offset = max(strfind(ceName{iName},'>'));
    if isempty(Offset), Offset = 0; end
    ceName{iName} = ceName{iName}(Offset+1:end);
end
 
legend(ceName,'Interpreter','None');

if ~bYreset
    set(gca,'YLim',YLim);
    set(gca,'XLim',XLim);
end

hFigure = gcf;

end


% == Automatic MEP detection
function [miMEP] = FindMEP(ceData,vFs,vOffset,Sens,StdMin,AmpMin,LatMin,bVerbose)

global SIZE_miEMG

if ~exist('bVerbose','var'), bVerbose = 1; end

% Load EMG template
ceEMG_template = which('EMG_template.mat','-ALL');
if numel(ceEMG_template) > 1
    warning(['Multiple occurence of EMG_template.mat file found ==> Please clean your matlab path. Loading ' ceEMG_template{1}]);
end
load(ceEMG_template{1});
vEMG = mean(mEMG,1);

% Init
miMEP = zeros(SIZE_miEMG,numel(ceData));

if bVerbose, hWtBar = waitbar(0,'Finding EMG peaks ...'); end

for iData = 1 : numel(ceData)
    
    % Resampling of EMG template if sampling frequencies differ
    vEMG_tmp = resample(vEMG,vFs(iData),FS);
    
    % MAX: index of EMG template's absolute maximum
    [Max, MAX] = max(abs(vEMG_tmp));
    
    % Time interval between min and max of the template
    [Max, iMax] = max(vEMG_tmp);
    [Min, iMin] = min(vEMG_tmp);
    MAXMININT = abs(iMax-iMin);
    WIDTH = MAXMININT;

    % Minimum latency of MEP: 15 ms
    LATMIN = ceil(LatMin * vFs(iData)) + vOffset(iData);
%     LATMIN = 0;
    
    
    % Std curve, by calculating std on a 12.5 ms moving window
    StdWidth = round(0.0125*vFs(iData));
    if rem(StdWidth,2) == 1, StdWidth = StdWidth+1; end % avoid odd number
    NbSample = numel(ceData{iData});
    vStd = zeros(1,NbSample-StdWidth);
    for iSample = StdWidth/2 : NbSample-StdWidth/2
        vStd(iSample-StdWidth/2+1) = std(ceData{iData}(iSample-StdWidth/2+1:iSample+StdWidth/2));
    end
    vStdDiff = diff(vStd); % First derivative of std curve
    
    % Finding local maxima of the filtered signal
    % Use 'findpeaks' function if possible (sig. proc. toolbox)
    if exist('findpeaks') == 2,
        
        [vPeaks, viSignalPk] = findpeaks(ceData{iData});
        
        % Cross-correlation product between signal and template
        vXcorr = xcorr(ceData{iData},vEMG_tmp);
        vXcorr = vXcorr ./ max(abs(vXcorr)); % Normalization (-1 -> 1)
        
        [vPeaks, viXcorrPkMax] = findpeaks(vXcorr); % Local max
        [vPeaks, viXcorrPkMin] = findpeaks(-vXcorr); % Local min
        try 
            viXcorrPks = sort([viXcorrPkMax viXcorrPkMin],'ascend'); 
        catch
            viXcorrPks = sort([viXcorrPkMax' viXcorrPkMin'],'ascend'); 
        end
        [vMax, viMax] = findpeaks(vXcorr,'sortstr','descend','MINPEAKHEIGHT',0.1); % Main local max
            
    % Otherwise use 'peakfinder'
    else
        [viSignalPk, vPeaks] = peakfinder(ceData{iData},0); 
        
        % Cross-correlation product between signal and template
        vXcorr = xcorr(ceData{iData},vEMG_tmp);
        vXcorr = vXcorr ./ max(abs(vXcorr)); % Normalization (-1 -> 1)
        
        [viXcorrPkMax, vPeaks] = peakfinder(vXcorr,0); % Local max
        [viXcorrPkMin, vPeaks] = peakfinder(-vXcorr,0); % Local min
        viXcorrPks = sort([viXcorrPkMax' viXcorrPkMin'],'ascend');
        [viMax, vMax] = peakfinder(vXcorr,0,0.1); % Main local max
        [vMax_sort, viMax_sort] = sort(vMax,1,'descend');
        viMax = viMax(viMax_sort);
    end
    
    
    % For each max, check wether it is a MEP or a false alarm
    for iMax = 1 : numel(viMax)
        
        % Max of X-correlation
        iPeak = find(viXcorrPks==viMax(iMax));
        % Corresponding time inex in data
        miMEP(1,iData) = viMax(iMax) - (numel(ceData{iData})-MAX);
        
        % Tests (see ref) on std surrounding this index 
        if (miMEP(1,iData)-round(MAX/2) <= LATMIN) || (miMEP(1,iData)+round(MAX/2) > numel(ceData{iData})) ...
                || (std(ceData{iData}(miMEP(1,iData)-WIDTH:miMEP(1,iData)+WIDTH)) < StdMin) || ...
                (miMEP(1,iData) > numel(ceData{iData}) ) 
            miMEP(1,iData) = 0;
            continue;
        end
        
        % Placing the marker Peak_1 on the real minimum of the data
        [DataMin, iDataMin] = min(ceData{iData}(miMEP(1,iData)-round(MAX/2):miMEP(1,iData)+round(MAX/2)));
        miMEP(1,iData) = miMEP(1,iData)-round(MAX/2)+iDataMin-1;
        
        % Min latency test
        if (miMEP(1,iData) < LATMIN)
            miMEP(1,iData) = 0;
            continue;
        end
        
        
        % Looking for the next max on the filtered data
        miMEP(2,iData) = viSignalPk(find(viSignalPk > miMEP(1,iData),1,'first'));
        try 
            % Looking for the real max of the data
            [DataMin, iDataMax] = max(ceData{iData}(miMEP(1,iData):miMEP(2,iData)+MAX));
        catch
            continue; % the marker is too close to the ritght edge
        end
        % Marker Peak_2
        miMEP(2,iData) = miMEP(1,iData)+iDataMax-1;
        
        if miMEP(2,iData) > numel(ceData{iData}), continue; end
        
        % Tests on the time interval between Peak_1 and 2
        MaxMinInt = miMEP(2,iData)-miMEP(1,iData);
        if  MaxMinInt < MAXMININT-2*abs(Sens)*MAXMININT || MaxMinInt > MAXMININT+2*abs(Sens)*MAXMININT || (abs(ceData{iData}(miMEP(2,iData))-ceData{iData}(miMEP(1,iData))) < AmpMin)
            miMEP(:,iData) = 0;
            continue;
        else
            % Placing the start and end markers based on the thresholding of
            % the first derivative of the std curve
            [Max,iMax] = max(vStdDiff(max(miMEP(1,iData)-MaxMinInt-StdWidth/2,1) : min(miMEP(1,iData)-StdWidth/2,numel(vStdDiff)))); % remontée avant le min
            [Min,iMin] = min(vStdDiff(max(miMEP(2,iData)+MaxMinInt-StdWidth/2,1) : min(miMEP(2,iData)+MaxMinInt-StdWidth/2,numel(vStdDiff)))); % descente après le max
            [iPre] = find(vStdDiff(1:iMax+miMEP(1,iData)-MaxMinInt-StdWidth/2) < 1e-5,1,'last') + StdWidth/2;
            [iPost] = find(vStdDiff(iMin+miMEP(2,iData)-StdWidth/2 : end) < 1e-5,1,'first') + iMin+miMEP(2,iData)+StdWidth/2;
            
            if isempty(iPre), continue; else miMEP(3,iData) = iPre; end
            if isempty(iPost), continue; else miMEP(4,iData) = iPost; end
            
            break;
        end
        
        
    end
    
    if bVerbose, waitbar(iData/numel(ceData),hWtBar); end

end

% TL index calculation
[miMEP] = TLiCalculation(miMEP,ceData,vEMG_tmp);

if bVerbose, close(hWtBar); end

end


% == Automatic detection of CSP
function [miCSP] = FindCSP(ceData,vFs,vOffset,Thres)

global SIZE_miEMG

NbData = numel(ceData);

miCSP = zeros(SIZE_miEMG,NbData);

if NbData >2, hWtBar = waitbar(0,'Finding CSP limits ...'); end

for iData = 1 : NbData
    
    % 1st derivative of the filtered signal, in mV.ms
    vData_filt_diff = abs(diff(ceData{iData})./(1/vFs(iData))) ./ 1e6;
    
    % Thresholding
    vThres = zeros(size(vData_filt_diff));
    vThres(vData_filt_diff < Thres) = 1;
    
    % Looking for the time period where first derivative is uneder the
    % threshold for at least 30ms
    vWin = rectwin(3*(1e-3)/(1/vFs(iData)));
    vThres = conv(vThres,vWin,'same');
    vThres = vThres./max(vThres);
    viPeriod = find(vThres == 1); 
    vPeriodLim = [1 find(diff(viPeriod)~=1) numel(viPeriod)];
    vPeriodLength = [diff(vPeriodLim)];
    
    [Max iMax] = max(vPeriodLength);
    
    while ((viPeriod(vPeriodLim(iMax)+1)-vOffset(iData)) / vFs(iData)) > 0.15 % if CSP is later than 150ms... 
        
        vPeriodLength(iMax) = 0;
        [Max iMax] = max(vPeriodLength);
        
    end
    
    % Start and end markers
    miCSP(3,iData) = viPeriod(vPeriodLim(iMax)+1);
    miCSP(4,iData) = viPeriod(vPeriodLim(iMax+1));
    
    
    
    if NbData >2, waitbar(iData/NbData,hWtBar); end
end

if NbData >2, close(hWtBar); end

end

% == Peak to peak amplitude calculation
function [vMEPAmp] = MEPAmpCalculation(ceData, miEMGTrig, BslMean, bRMS, cNoMEP)

for iData = 1 : numel(ceData)
    if miEMGTrig(1,iData) == 0
        if strcmp(cNoMEP,'NaN'), vMEPAmp(iData) = NaN; elseif strcmp(cNoMEP,'Zero'), vMEPAmp(iData) = 0;  end;
        continue;
    end
    if ~bRMS
        if isempty(BslMean),
            vMEPAmp(iData) = abs(ceData{iData}(miEMGTrig(1,iData)) - ceData{iData}(miEMGTrig(2,iData)));
        else
            vMEPAmp(iData) = 100*abs(ceData{iData}(miEMGTrig(1,iData)) - ceData{iData}(miEMGTrig(2,iData)))/BslMean;
        end
    else
        if miEMGTrig(3,iData) == 0 || miEMGTrig(4,iData) == 0
            vMEPAmp(iData) = NaN;
            continue;
        end
        vTmp = ceData{iData}(miEMGTrig(3,iData) : miEMGTrig(4,iData));
        if isempty(BslMean),
            vMEPAmp(iData) = sqrt(mean(vTmp)^2 + std(vTmp)^2);
        else
            vMEPAmp(iData) = 100*sqrt(mean(vTmp)^2 + std(vTmp)^2)/BslMean;%!!!
        end
    end
end
end

% == TL index calculation
function [miMEP] = TLiCalculation(miMEP,ceData,vEMG_tmp)

if isempty(vEMG_tmp)
    
    ceEMG_template = which('EMG_template.mat','-ALL');
    if numel(ceEMG_template) > 1
        warning(['Multiple occurence of EMG_template.mat file found ==> Please clean your matlab path. Loading ' ceEMG_template{1}]);
    end
    load(ceEMG_template{1});
    
    vEMG_tmp = mean(mEMG,1); % EMG template
    
end

for iData = 1 : numel(ceData)
    
    if any(miMEP(1:2,iData) == 0)
        continue;
    else
        
        [Max, iMax] = max(vEMG_tmp);
        [Min, iMin] = min(vEMG_tmp);
        MAXMININT = abs(iMax-iMin);
        MaxMinInt = miMEP(2,iData)-miMEP(1,iData);
        
        % Fitting (using resampling) the time interval between peak_1 and peak_2 with on the
        % EMG template's one 
        try
            vData_res = resample(ceData{iData},MAXMININT,MaxMinInt);
            
            % découpage signal de même longeur que template
            [Max, iMax] = max(vEMG_tmp);
            [Min, iMin] = min(vEMG_tmp);
            NbTmp = numel(vEMG_tmp);
            
            % Starting index on signal
            iStart = round(miMEP(1,iData)*MAXMININT/MaxMinInt)-iMin;
            if iStart >= 1,
                % Epoching signal to have the same size than the EMG template
                vPA_norm = vData_res(iStart : min(round(miMEP(2,iData)*MAXMININT/MaxMinInt)+(NbTmp-iMax-1),numel(vData_res))); %min : si on est aux extremes
                % Normalized X-correlation score
                miMEP(5,iData) = max(xcorr(vEMG_tmp(1:numel(vPA_norm)),vPA_norm,'coeff')*100);
               
            % If starting index is negative, the size of the EMG signal is fitted
            else
                vPA_norm = vData_res(1 : min(round(miMEP(2,iData)*MAXMININT/MaxMinInt)+(NbTmp-iMax-1),numel(vData_res)));
                miMEP(5,iData) = max(xcorr(vEMG_tmp(2+abs(iStart):end),vPA_norm,'coeff')*100);
            end
        catch
            % peak 1 might be after peak 2...
            try
                MaxMinInt = miMEP(1,iData)-miMEP(2,iData);
                vData_res = resample(-ceData{iData},MAXMININT,MaxMinInt);
                
                % découpage signal de même longeur que template
                [Max, iMax] = max(vEMG_tmp);
                [Min, iMin] = min(vEMG_tmp);
                NbTmp = numel(vEMG_tmp);
                
                % Starting index on signal
                iStart = round(miMEP(1,iData)*MAXMININT/MaxMinInt)-iMin;
                if iStart >= 1,
                    % Epoching signal to have the same size than the EMG template
                    vPA_norm = vData_res(iStart : min(round(miMEP(2,iData)*MAXMININT/MaxMinInt)+(NbTmp-iMax-1),numel(vData_res))); %min : si on est aux extremes
                    % Normalized X-correlation score
                    miMEP(5,iData) = max(xcorr(vEMG_tmp(1:numel(vPA_norm)),vPA_norm,'coeff')*100);
                    
                    % If starting index is negative, the size of the EMG signal is fitted
                else
                    vPA_norm = vData_res(1 : min(round(miMEP(2,iData)*MAXMININT/MaxMinInt)+(NbTmp-iMax-1),numel(vData_res)));
                    miMEP(5,iData) = max(xcorr(vEMG_tmp(2+abs(iStart):end),vPA_norm,'coeff')*100);
                end
            catch
                miMEP(5,iData) = NaN;
                continue;
            end
        end
    end
end
end

% == Marker modification function: modify or remove
function [miEMGTrig] = ModifyEMGTrig(ceTime,ceData,miEMGTrig,bRemove,hFigure)

global SIZE_miEMG

% User select the point to be modified by clicking
figure(hFigure);
[X,Y] = ginput(1);

NbEMG = size(miEMGTrig,2);
viEMGTrig = reshape(miEMGTrig,1,NbEMG*SIZE_miEMG);

% Detection of the marker to be modified (min distance between marker and
% the user's click coordinates)
kEMG = 1;
for iData = 1 : numel(ceData)
    
    % si pas de marqueur EMG trouve, dist = Inf
    if viEMGTrig(kEMG) == 0, vDist(kEMG) = +Inf; else vDist(kEMG) = sqrt((ceTime{iData}(viEMGTrig(kEMG))-X).^2 + (ceData{iData}(viEMGTrig(kEMG))-Y).^2); end
    if viEMGTrig(kEMG+1) == 0, vDist(kEMG+1) = +Inf; else vDist(kEMG+1) = sqrt((ceTime{iData}(viEMGTrig(kEMG+1))-X).^2 + (ceData{iData}(viEMGTrig(kEMG+1))-Y).^2); end
    if viEMGTrig(kEMG+2) == 0, vDist(kEMG+2) = +Inf; else vDist(kEMG+2) = sqrt((ceTime{iData}(viEMGTrig(kEMG+2))-X).^2 + (ceData{iData}(viEMGTrig(kEMG+2))-Y).^2); end
    if viEMGTrig(kEMG+3) == 0, vDist(kEMG+3) = +Inf; else vDist(kEMG+3) = sqrt((ceTime{iData}(viEMGTrig(kEMG+3))-X).^2 + (ceData{iData}(viEMGTrig(kEMG+3))-Y).^2); end
    
    kEMG = kEMG+SIZE_miEMG;
end
vDist(vDist==0) = +inf;
[Min, iMin] = min(vDist);
iData = ceil(iMin/SIZE_miEMG);

% Displaying point to be modified
gca; hold on;
hSelec = plot(ceTime{iData}(viEMGTrig(iMin)),ceData{iData}(viEMGTrig(iMin)),'om');
hSelec = plot(ceTime{iData}(viEMGTrig(iMin)),ceData{iData}(viEMGTrig(iMin)),'*w');

% Replacement...
if ~bRemove
    % New coordinates selected by the user
    [X,Y] = ginput(1);
    [Min viEMGTrig(iMin)] = min(abs(ceTime{iData}-X));
    
%... or supression
else
    switch rem(iMin,SIZE_miEMG)
        case 1
            viEMGTrig(iMin:iMin+4) = 0;
        case 2
            viEMGTrig(iMin-1:iMin+3) = 0;
        case 3
            viEMGTrig(iMin-2:iMin+2) = 0;
        case 4
            viEMGTrig(iMin-3:iMin+1) = 0;
        case 0
            viEMGTrig(iMin-4:iMin) = 0;
    end
end

miEMGTrig = reshape(viEMGTrig,SIZE_miEMG,NbEMG);

end

% == Add marker
function [miEMGTrig] = AddEMGTrig(ceTime,ceData,miEMGTrig,hFigure,cPoint);

% User select the EMG signal where to put the new marker
figure(hFigure);
[X,Y] = ginput(1);

NbEMG = size(miEMGTrig,2);
Fs = 1/diff(ceTime{1}(1:2));

% Finding the curve
for iData = 1 : numel(ceData)
    iSample = find(ceTime{iData}>=X,1,'first');
    vDist(iData) = abs(ceData{iData}(iSample)-Y);
end
[Min, iData] = min(vDist);

switch cPoint
    case 'Min'
        % If a marker is already present, stop here and ask user to use the modify function instead 
        if miEMGTrig(1,iData) ~= 0,
            warndlg('The selected curve allready has an EMGTrig, please use "modify EMG trig" function instead', 'Warning');
            miEMGTrig = [];
            return;
        end
        iSample = find(ceTime{iData}>=X,1,'first');
        [Min, iMin] = min(ceData{iData}(iSample-round(0.01*Fs):iSample+round(0.01*Fs))); % looking at +/- 10 ms
        miEMGTrig(1,iData) = iSample-round(0.01*Fs)+iMin-1;
        cPlot = '*r';
        
    case 'Max'
        % If a marker is already present, stop here and ask user to use the modify function instead 
        if miEMGTrig(2,iData) ~= 0,
            warndlg('The selected curve allready has an EMGTrig, please use "modify EMG trig" function instead', 'Warning');
            miEMGTrig = [];
            return;
        end
        iSample = find(ceTime{iData}>=X,1,'first');
        [Max, iMax] = max(ceData{iData}(iSample-round(0.01*Fs):iSample+round(0.01*Fs)));
        miEMGTrig(2,iData) = iSample-round(0.01*Fs)+iMax-1;
        cPlot = '*b';
        
    case 'Start'
        % If a marker is already present, stop here and ask user to use the modify function instead 
        if miEMGTrig(3,iData) ~= 0,
            warndlg('The selected curve allready has an EMGTrig, please use "modify EMG trig" function instead', 'Warning');
            miEMGTrig = [];
            return;
        end
        iSample = find(ceTime{iData}>=X,1,'first');
        miEMGTrig(3,iData) = iSample;
        cPlot = '*g';
        
    case 'End'
        % If a marker is already present, stop here and ask user to use the modify function instead 
        if miEMGTrig(4,iData) ~= 0,
            warndlg('The selected curve allready has an EMGTrig, please use "modify EMG trig" function instead', 'Warning');
            miEMGTrig = [];
            return;
        end
        iSample = find(ceTime{iData}>=X,1,'first');
        miEMGTrig(4,iData) = iSample;
        cPlot = '*g';
end



end

% == Converting data coming from protocol (conditions in random order)
function [sData, sEMG] = ProtocolConvert(sData, sEMG, cProtoFile)

% Reading the protocol definition file
[ceCondNum ceCondName ceProtoName ceCondNameNS ceProtoNameNS] = ReadProtoFile(cProtoFile);

% Init
sData_proto(1).Side=[];
sData_proto(1).cName = [];
sData_proto(1).ceData={};
sData_proto(1).vFs=[];
sData_proto(1).ceTime={};
sData_proto(1).vOffset=[];
sData_proto(1).ceFile={};
sData_proto(1).vOrder=[];

viDataProt = [];
for iData = 1 : numel(sData)
    
    % No side info
    if sData(iData).Side == -1,
        
        iProto = find(strcmp(ceProtoNameNS,sData(iData).cName));
        if isempty(iProto),      continue;     end
        
        for iCond =  1 : numel(ceCondNameNS), sData_proto(iCond).cName = ceCondNameNS{iCond}; end
        
    % with side info
    else

        iProto = find(strcmp(ceProtoName,sData(iData).cName));
        if isempty(iProto),      continue;    end
        
        for iCond =  1 : numel(ceCondName), sData_proto(iCond).cName = ceCondName{iCond}; end
        
        % Looking for the protocol's side
        if sData(iData).Side == 0,
            for iCond = 1 : numel(ceCondName)/2, sData_proto(iCond).Side = sData(iData).Side;  end
        else
            for iCond = numel(ceCondName)/2+1 : numel(ceCondName), sData_proto(iCond).Side = sData(iData).Side;  end
        end
    end

    % Rearanging the data structure
    for iEMG = 1 : numel(sData(iData).ceData)
        sData_proto(ceCondNum{iProto}(iEMG)).ceData{end+1} = sData(iData).ceData{iEMG};
        sData_proto(ceCondNum{iProto}(iEMG)).vFs(end+1) = sData(iData).vFs(iEMG);
        sData_proto(ceCondNum{iProto}(iEMG)).ceTime{end+1} = sData(iData).ceTime{iEMG};
        sData_proto(ceCondNum{iProto}(iEMG)).vOffset(end+1) = sData(iData).vOffset(iEMG);
        sData_proto(ceCondNum{iProto}(iEMG)).ceFile{end+1} = sData(iData).ceFile{iEMG};
        sData_proto(ceCondNum{iProto}(iEMG)).vOrder(end+1) = sData(iData).vOrder(iEMG);
    end
    viDataProt(end+1) = iData;
end

sData(viDataProt) = [];

% Empty protocol suppression
viProto = [];
for iProto = 1 : numel(sData_proto)
    if isempty(sData_proto(iProto).ceData), viProto(end+1) = iProto; end
end
sData_proto(viProto) = [];

sData(end+1:end+numel(sData_proto)) = sData_proto;

end


% == Loading marker file 
function[sData sEMG ceSide] = LoadData(cPath,sData)

global SIZE_miEMG

% Init
ceSide = {[], []};

% Loading marker file if available...
try
    sDir = dir(cPath);
    if isfield(sData,'cEMGChan')
        for iFile = 1 : numel(sDir)
            if ~isempty(strfind(sDir(iFile).name,['EMG_trig_' sData.cEMGChan ]))
                break;
            end
        end
        if iFile == numel(sDir), % si rien trouvé on tente sans extension
            for iFile = 1 : numel(sDir)
                if ~isempty(strfind(sDir(iFile).name,['EMG_trig.mat']))
                    break;
                end
            end
            if iFile == numel(sDir), iFile =[]; end % si rien trouvé
        end 
    else
        for iFile = 1 : numel(sDir)
            if ~isempty(strfind(sDir(iFile).name,['EMG_trig.mat']))
                break;
            end
        end
        if iFile == numel(sDir), iFile =[]; end % si rien trouvé 
    end
    load([cPath filesep sDir(iFile).name]);
    
    % Toolbox version
    if ~exist('MEP_Toolbox_Ver'),  MEP_Toolbox_Ver = 0; end
    if isempty(MEP_Toolbox_Ver), MEP_Toolbox_Ver = 1; end
    
    % Convert data coming from protocols
    iProtoFile = find(strcmp({sDir.name},'protocol_def.txt'));
    if ~isempty(iProtoFile),
        if MEP_Toolbox_Ver == 0
            [sData sEMG] = ProtocolConvert(sData, sEMG,[]);
        else
            [sData] = ProtocolConvert(sData,[],[cPath filesep sDir(iProtoFile).name]);
        end
    end
    
    % Backward compatibility
    if MEP_Toolbox_Ver <= 1
        for iEMG = 1 : numel(sEMG)
            sEMG(iEMG).miEMG = [sEMG(iEMG).miEMG ; zeros(SIZE_miEMG-size(sEMG(iEMG).miEMG,1),size(sEMG(iEMG).miEMG,2))];
        end
    end
    
    % Display artifacts in red
    for iData = 1 : numel(sData)
        for iFile = 1 : numel(sData(iData).ceFile)
            if sEMG(iData).miEMG(6,iFile) == -1,
                sData(iData).ceFile{iFile} = regexprep(sData(iData).ceFile{iFile},'"black"', '"red"'); 
            end
        end
    end
    
%... Init miEMG to 0 otherwise
catch
    iProtoFile = find(strcmp({sDir.name},'protocol_def.txt'));
    if ~isempty(iProtoFile), [sData] = ProtocolConvert(sData,[],[cPath filesep sDir(iProtoFile).name]); end
    for iData = 1 : numel(sData)
        sEMG(iData).miEMG = zeros(SIZE_miEMG,numel(sData(iData).ceData));
    end
    ceSide = {[], []};
end

hWb = waitbar(0,'Filtering...');
for iData = 1: numel(sData)
    for iEMG = 1 : numel(sData(iData).ceData)
        % Filtering, if Fs >= 2kHz
    if  sData(iData).vFs(iEMG) >= 2000
        

        %DC offset
        sData(iData).ceData{iEMG} = sData(iData).ceData{iEMG} - mean(sData(iData).ceData{iEMG});
        
        % 0 padding
        EMGsize = numel(sData(iData).ceData{iEMG});
        sData(iData).ceData{iEMG} = [zeros(1,EMGsize) sData(iData).ceData{iEMG}' zeros(1,EMGsize) ];
        
        
        %High pass
        [N, Wn] = buttord(5/(sData(iData).vFs(iEMG)/2), 10/(sData(iData).vFs(iEMG)/2), 3, 20); % 5 -10 Hz
        [B,A] = butter(N,Wn,'high');
        sData(iData).ceData{iEMG} = filtfilt(B,A,double(sData(iData).ceData{iEMG}));
        
        % Low pass
        [N, Wn] = buttord(600/(sData(iData).vFs(iEMG)/2), 800/(sData(iData).vFs(iEMG)/2), 3, 20); %600 -800 Hz
        [B,A] = butter(N,Wn);
        sData(iData).ceData{iEMG} = filtfilt(B,A,double(sData(iData).ceData{iEMG}));
        
        
       % Windowing
       sData(iData).ceData{iEMG} = sData(iData).ceData{iEMG}(EMGsize+1:2*EMGsize);
    else
        waitfor(warndlg('Filtering aborded. Please use data sampled at a minimum of 2kHz.'));
        return;
    end
    end
    waitbar(iData/numel(sData),hWb);
end
close(hWb);

end

% == Sorting conditions for group analysis
function [ceData, ceCond] = SortCond(ceData)

    ceCond = {};
    Cond = 0;
    for iData = 1 : numel(ceData)
        for iCond = 1 : numel(ceData{iData})
            
            [i iCondFind] = find(strcmp(ceCond,['<HTML><BODY color="black">' ceData{iData}(iCond).cName]));
            
            % New condition found
            if isempty(iCondFind)
                Cond = Cond + 1;
                ceData{iData}(iCond).iCond = Cond;
                ceCond{1,end+1} = ['<HTML><BODY color="black">' ceData{iData}(iCond).cName];
                ceCond{2,end} = Cond;
            
            % Condition already found
            else
                ceData{iData}(iCond).iCond = iCondFind;
            end
        end
    end
end

% == Rearanging conditions
function [ceData, ceCond] = RearrangeCond(ceData, ceCond, viCond, ceName)

ceCond{1,viCond(1)} = ['<HTML><BODY color="black">' ceName{1}];

for iCond = 2 : numel(viCond)
    for iData = 1 : numel(ceData)
        viOldCond = find([ceData{iData}(:).iCond] == ceCond{2,viCond(iCond)});
        for iOldCond = viOldCond, ceData{iData}(iOldCond).iCond =  ceCond{2,viCond(1)}; end
        
    end
end

ceCond(:,viCond(2:end)) = [];

end

% == Individual statistics (mean and std of selected features)
function [sIndStat] = ComputeIndStat(ceData,ceEMG,ceCond,iBslCond,cMeasure,cMethod)

for iData = 1 : numel(ceData)
    
    % Baseline calculation if needed
    if ~isempty(iBslCond),
        BslCond = ceCond{2,iBslCond};
        vBsl = [];
        viBsl = find([ceData{iData}(:).iCond] == BslCond);
        
        switch cMeasure
            case 'Amp'
                for iBsl = viBsl
                    [vBsl] = [vBsl MEPAmpCalculation(ceData{iData}(iBsl).ceData, ceEMG{iData}(iBsl).miEMG, [],0,'NaN')];
                end
            case 'TLi'
                for iBsl = viBsl
                    [vBsl] = [vBsl ceEMG{iData}(iBsl).miEMG(5,:)];
                end
                vBsl(vBsl==0) = NaN; % -> 0 : pas de MEP
            case 'RMS'
                for iBsl = viBsl
                    [vBsl] = [vBsl MEPAmpCalculation(ceData{iData}(iBsl).ceData, ceEMG{iData}(iBsl).miEMG, [],1,'NaN')];
                end
            case 'Lat'
                for iBsl = viBsl
                    [vBsl] = [vBsl 1000*(ceEMG{iData}(iBsl).miEMG(3,:)-ceData{iData}(iBsl).vOffset) ./ ceData{iData}(iBsl).vFs]; %en ms
                end
            case 'Dur'
                for iBsl = viBsl
                    vTime =  ceEMG{iData}(iBsl).miEMG(4,:)-ceEMG{iData}(iBsl).miEMG(3,:);
                    [vBsl] = [vBsl 1000.*vTime ./ ceData{iData}(iBsl).vFs]; % en ms
                end
        end
        
        switch cMethod
            case 'All'
                BslMean = nanmean(vBsl);
            case '5Bests'
                vBsl = sort(vBsl);
                vBsl(isnan(vBsl)) = [];
                BslMean = mean(vBsl(max(end-4,1):end));
            case 'Med'
                BslMean = nanmedian(vBsl);
        end
    else
       BslMean = []; 
    end
    
    for iCond = 1:length(ceCond)
        
        vCond = [];
        viCond = find([ceData{iData}(:).iCond] == ceCond{2,iCond});
        
        switch cMeasure
            case 'Amp'
                for iC = viCond
                    [vCond] = [vCond MEPAmpCalculation(ceData{iData}(iC).ceData, ceEMG{iData}(iC).miEMG, BslMean,0,'NaN')];
                end
            case 'TLi'
                if isempty(BslMean)
                    for iC = viCond
                        [vCond] = [vCond ceEMG{iData}(iC).miEMG(5,:)];
                    end
                else % Baseline normalization
                    for iC = viCond
                        [vCond] = [vCond 100.*(ceEMG{iData}(iC).miEMG(5,:)/BslMean)];
                    end
                end
                vCond(vCond==0) = NaN; % -> 0 : no MEP
            case 'RMS'
                for iC = viCond
                    [vCond] = [vCond MEPAmpCalculation(ceData{iData}(iC).ceData, ceEMG{iData}(iC).miEMG, BslMean,1,'NaN')];
                end
            case 'Lat'
                if isempty(BslMean)
                    for iC = viCond
                        [vCond] = [vCond 1000*(ceEMG{iData}(iC).miEMG(3,:)-ceData{iData}(iC).vOffset) ./ ceData{iData}(iC).vFs]; %en ms
                    end
                else
                    for iC = viCond
                        [vCond] = [vCond 100*(1000*(ceEMG{iData}(iC).miEMG(3,:)-ceData{iData}(iC).vOffset) ./ ceData{iData}(iC).vFs)/BslMean]; %en %
                    end
                end
            case 'Dur'
                if isempty(BslMean)
                    for iC = viCond
                        vTime =  ceEMG{iData}(iC).miEMG(4,:)-ceEMG{iData}(iC).miEMG(3,:);
                        [vCond] = [vCond 1000.*vTime ./ ceData{iData}(iC).vFs]; % en ms
                    end
                else
                    for iC = viCond
                        vTime =  ceEMG{iData}(iC).miEMG(4,:)-ceEMG{iData}(iC).miEMG(3,:);
                        [vCond] = [vCond 100*(1000.*vTime ./ ceData{iData}(iC).vFs)/BslMean]; % en %
                    end
                end

        end

        switch cMethod
            case 'All'
                sIndStat(iData).mMeanStd(1,iCond) = nanmean(vCond);
                sIndStat(iData).mMeanStd(2,iCond) = nanstd(vCond);
            case '5Bests'
                vCond = sort(vCond);
                vCond(isnan(vCond)) = [];
                sIndStat(iData).mMeanStd(1,iCond) = mean(vCond(max(end-4,1):end));
                sIndStat(iData).mMeanStd(2,iCond) = std(vCond(max(end-4,1):end));
            case 'Med'
                sIndStat(iData).mMeanStd(1,iCond) = nanmedian(vCond);
                sIndStat(iData).mMeanStd(2,iCond) = nanstd(vCond);
            case 'Raw'
                sIndStat(iData).mMeanStd(1,iCond) = nanmean(vCond);
                sIndStat(iData).mMeanStd(2,iCond) = nanstd(vCond);
        end
        sIndStat(iData).ceTrl{iCond} = vCond;
    end

end
end




