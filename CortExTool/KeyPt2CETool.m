function [sData, cPatient] = KeyPt2CETool(cPath)

% function [sData] = KeyptExport2Matlab(cPath)
%
% Converts EMG data coming from Keypoint (Natus med. inc.) system into
% matlab structure.
%
%   INPUT : - cPath:[STR] folder containing Keypoint data
%   OUTPUTS : - sData:[STRUCT] matlab structure
%            - cPatient:[STR] patient name

% Copyright (C) 2015  Sylvain Harquel, CNRS
%
% This file is part of CortExTool.
% 
% CortExTool is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% CortExTool is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with CortExTool.  If not, see <http://www.gnu.org/licenses/>


sDir = dir(cPath);
if isempty(sDir) || numel(sDir)==2 || isempty(find(strcmp({sDir.name},'Study.txt')))
    sData = [];
    cPatient = [];
    return
end

% Study file containing patient's name
iStudy = find(strcmp({sDir.name},'Study.txt'));
cPatient = ReadStudyFile([cPath filesep sDir(iStudy).name]);

% Reading data
iStim = 1;
for iDir = 1 : numel(sDir)
    if sDir(iDir).name(1) == '.', continue; end
    if sDir(iDir).isdir
        sDir2 = dir([cPath filesep sDir(iDir).name]); % 1st level: Condition (ex: BASELINE_120)
        
        for iDir2= 1 : numel(sDir2)
            if sDir2(iDir2).name(1) == '.', continue; end
            if sDir2(iDir2).isdir
                sDir3 = dir([cPath filesep sDir(iDir).name filesep sDir2(iDir2).name]); % 2nd level: Side (ex: Left)
                
                for iDir3= 1 : numel(sDir3)
                    if sDir3(iDir3).name(1) == '.', continue; end 
                    if sDir3(iDir3).isdir
                        
                        cPath_tmp = [cPath filesep sDir(iDir).name filesep sDir2(iDir2).name filesep sDir3(iDir3).name]; 
                        sDir4 = dir(cPath_tmp); % 3rd level: EMG files
                        
                        if numel(sDir4) == 2, continue; end                         

                        
                        if strcmp(sDir3(iDir3).name,'Left') %1: R, 0: L
                            sData(iStim).Side = 0;
                            % Condition name and side
                            sData(iStim).cName = [sDir(iDir).name '_L'];
                        elseif strcmp(sDir3(iDir3).name,'Right')
                            sData(iStim).Side = 1; 
                            sData(iStim).cName = [sDir(iDir).name '_R'];
                        else
                            sData(iStim).Side = -1; 
                            sData(iStim).cName = sDir(iDir).name;
                        end
                        
                        % Reading header file
                        iHeader = find(strcmp({sDir4.name},'Header.txt'));
                        sParam = ReadHeaderFile([cPath_tmp filesep sDir4(iHeader).name]);
                        
                        % Reading the EMG files in header's order
                        for iFile = 1 : numel(sParam)
                            iEMG = find(strcmp({sDir4.name},sParam(iFile).cFile));
                            sData(iStim).ceData{iFile} = load([cPath_tmp filesep sDir4(iEMG).name]); % data
                            sData(iStim).vFs(iFile) = sParam(iFile).Fs; % Fs
                            sData(iStim).ceTime{iFile} = ([1 : numel(sData(iStim).ceData{iFile})] - sParam(iFile).Offset) ./ sData(iStim).vFs(iFile); % time vector
                            Offset = find(sData(iStim).ceTime{iFile}==0); % sample offset
                            if isempty(Offset) || Offset==1, sData(iStim).vOffset(iFile) = 0; else sData(iStim).vOffset(iFile) = Offset; end %
                            sData(iStim).ceFile{iFile} = ['<HTML><BODY color="black">' sParam(iFile).cFile]; % Nom fichier EMG
                            sData(iStim).vOrder(iFile) = iFile;
                        end
                        
                        % Next stim/condition
                        iStim = iStim+1;
                    else
                        continue;
                    end
                end
            else
                continue;
            end
        end
    else
        continue;
    end
end







