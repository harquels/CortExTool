function [ceCondNum, ceCondName, ceProtoName, ceCondNameNS, ceProtoNameNS] = ReadProtoFile(cFile)

% function [sData] = KeyptExport2Matlab(cPath)
%
% Read CortExTool protocol file.
%
%   INPUT : - cFile:[STR] protocol definition file
%   OUTPUTS : - ceCondNum:[CELL] condition index for each protocol
%            - ceCondName:[STR] condition names
%            - ceProtoName:[CELL] protocol names

% Copyright (C) 2015  Sylvain Harquel, CNRS
%
% This file is part of CortExTool.
% 
% CortExTool is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% CortExTool is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with CortExTool.  If not, see <http://www.gnu.org/licenses/>

ceProto = textread(cFile,'%s');

NbProto = str2num(ceProto{1});
if isempty(NbProto), 
    return;
end

% Reading file, finding all (unique) conditions
for iProto =  1 : NbProto
    vNbEMG(iProto) = str2num(ceProto{iProto+1});
    %without side info
    ceProtoNameNS{iProto} = [ceProto{NbProto+1+iProto+sum(vNbEMG(1:iProto-1))}];
    %with side info
    if iProto > 1, 
        ceProtoName{iProto} = [ceProto{NbProto+1+iProto+sum(vNbEMG(1:iProto-1))} '_L'];
        ceProtoName{NbProto+iProto} = [ceProto{NbProto+1+iProto+sum(vNbEMG(1:iProto-1))} '_R'];
        for iEMG = 1 : vNbEMG(iProto)
            ceEMG{iProto}{iEMG} = ['Proto_' ceProto{NbProto+1+iProto+sum(vNbEMG(1:iProto-1))+iEMG}];
        end
        ceCondNameNS = union(ceCondNameNS,unique(ceEMG{iProto}));
    else
        ceProtoName{iProto} = [ceProto{NbProto+2} '_L'];
        ceProtoName{NbProto+iProto} = [ceProto{NbProto+2} '_R'];
        for iEMG = 1 : vNbEMG(iProto)
            ceEMG{iProto}{iEMG} = ['Proto_' ceProto{NbProto+2+iEMG}];
        end
        ceCondNameNS = unique(ceEMG{iProto});
    end
end


% Condition index for each protocol
for iProto = 1 : NbProto
    for iEMG = 1 : vNbEMG(iProto)
        ceCondNum{iProto}(iEMG) = find(strcmp(ceCondNameNS,ceEMG{iProto}{iEMG}));
    end
end

% Duplication for left AND right sides
ceCondNum(end+1 : end+NbProto) = ceCondNum(1:end);
for iProto = 1 : NbProto, ceCondNum{NbProto+iProto} = ceCondNum{iProto} + numel(ceCondNameNS); end

NbCond = numel(ceCondNameNS);
for iCond =  1 : NbCond
    cName = ceCondNameNS{iCond};
    ceCondName{iCond} = [cName '_L'];
    ceCondName{iCond+NbCond} = [cName '_R'];
end