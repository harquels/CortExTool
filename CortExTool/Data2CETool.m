function [sData, cPatient] = Data2CETool(cPath, cType)

% function [sData] = Data2CETool(cPath)
%
% Converts EMG data (Spike2 .mat, EDF .edf) into a CETool matlab structure. 
% Spike2 data must be exported in matlab ".mat" format.
%
%   INPUT : - cPath:[STR] folder containing data
%           - cType:[STR] data fromat ('Sp2' or 'edf')
%   OUTPUTS : - sData:[STRUCT] matlab structure
%            - cPatient:[STR] patient name

% Copyright (C) 2015  Sylvain Harquel, CNRS
%
% This file is part of CortExTool.
% 
% CortExTool is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% CortExTool is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with CortExTool.  If not, see <http://www.gnu.org/licenses/>

sDir = dir(cPath);
if isempty(sDir) || numel(sDir)==2
    sData = [];
    cPatient = [];
    return
end

% Study file containing patient's name
if ~isempty(find(strcmp({sDir.name},'Study.txt'))), 
    iStudy = find(strcmp({sDir.name},'Study.txt'));
    cPatient = ReadStudyFile([cPath filesep sDir(iStudy).name]);
else
    cPatient = 'Test';
end

% Reading data
iStim = 1;
for iDir = 1 : numel(sDir)
    if sDir(iDir).name(1) == '.', continue; end % 1 et 2 = dossiers . et ..
    if sDir(iDir).isdir
        
        cPath_tmp = [cPath filesep sDir(iDir).name];
        
        if strcmp(sDir(iDir).name(end-1:end),'_L') %1: cote droit, 0 : cote gauche
            sData(iStim).Side = 0; %
        elseif strcmp(sDir(iDir).name(end-1:end),'_R')
            sData(iStim).Side = 1; %
        else
            sData(iStim).Side = -1; %
        end
        cDirName = sDir(iDir).name;
        sData(iStim).cName = cDirName;
        
        % Opening data
        % !!! TO DO : Multiple files  !!!
        switch cType
            case {'Sp2','Sp2_bis'}
                
                load([cPath_tmp filesep sData(iStim).cName]);
                
                % looking if we have a "wave_data" 
                sVar = whos;
                ceSearch = strfind({sVar.name},'wave_data');
                iVar=[];
                for iS = 1 : numel(ceSearch), 
                    if isempty(ceSearch{iS}), continue; else iVar = iS; break;  end  
                end
                
                %either one struct for all channels
                if ~isempty(iVar) % if one var is named "wave_data"
                    cType = 'Sp2_bis';
                    eval(['wave_data = ' sVar(iVar).name ';']);
                    ceChanName =  {wave_data.chaninfo.title};
                % or une struct per channel (depending on exporting options)
                else
                    sChannels = load([cPath_tmp filesep sData(iStim).cName]);
                    ceChanName = fieldnames(sChannels);
                end
                
        
            case 'edf'
                try 
                    [sHdr mData] = edfread([cPath_tmp filesep sData(iStim).cName '.edf']);
                catch
                   sDir2 = dir(cPath_tmp);
                   [sHdr mData] = edfread([cPath_tmp filesep sDir2(3).name]);
                end
                ceChanName = sHdr.label;
                
            case 'mat'
                load([cPath_tmp filesep sData(iStim).cName]);
                for iChan = 1 : size(labels,1), ceChanName{iChan} = labels(iChan,:); end
                if size(data,1)>size(data,2), mData = data'; else mData = data; end
                
        end
        
        % choix events possible :
        ceChanName{end+1} = 'Events list (opt.) :';
        
        % Selection of EMG and trigger channels
        if ~strcmp(cType,'LabV') && (iStim == 1 || strcmp(cChanAns,'No'))
            iEMGChan = [];
            iTrigChan = [];
            while numel(iEMGChan)~=1 && numel(iTrigChan)~=1,
                viEMGChan = inputdlg(ceChanName,[sData(iStim).cName ': c=chan, r=ref (opt.), 1,2=trigmode 1,2,...'],[1 50]);
                if isempty(viEMGChan)
                    sData = [];
                    return
                end
                iEMGChan = find(strcmp(viEMGChan,'c'));
                iRefChan = find(strcmp(viEMGChan,'r'));
                iTrigChan = find(strcmp(viEMGChan,'1'));
                viEvents = str2num(viEMGChan{end});
                if numel(iTrigChan)~=1, iTrigChan = find(strcmp(viEMGChan,'2')); end

                if numel(iTrigChan) > 1,
                    waitfor(warndlg('Please select only one Trigger channel.')); 
                end
                if numel(iEMGChan)~=1,
                    waitfor(warndlg('Please select only one EMG channel.')); 
                end
                
                TrigMode = str2double(viEMGChan(iTrigChan));
            end
            sData.cEMGChan = ceChanName{iEMGChan};
            cChanAns = questdlg('Select the same EMG and trigger channels through all conditions ?', 'Channels', 'Yes', 'No','Yes');
        end
        
        % Conversion
        switch cType
            case 'Sp2'
        
                eval(['sEMG = sChannels.' ceChanName{iEMGChan} ';']);
                eval(['sTrig = sChannels.' ceChanName{iTrigChan} ';']);
                
                % Sampling freq
                Fs = 1/sEMG.interval;
                
                % Finding trial onsets
                % 1. DIGITAL trigger input (should be only 0 and 1)
                if isfield(sTrig,'values')
                    vTrl = find(sTrig.values~=0);
                % 2. TIMES input
                elseif isfield(sTrig,'times')
                    vTrl = round((sTrig.times - sEMG.times(1))*Fs);
                else
                    errordlg(['Unrecognized data format. Please contact sylvain dot harquel at univ-grenoble-alpes dot fr.']);
                end
                
                % TO DO !!!
                % Time window limits : from -300 to +300ms by
                % default !!!
                vWin = floor([-0.3 0.3].*Fs);
                vTime = [-0.3 : 1/Fs : 0.3];

                
                % Filling cortextool data structure
                for iTrl = 1 : numel(vTrl)
                    
                    sData(iStim).ceData{iTrl} = sEMG.values([vWin(1) : vWin(2)]+vTrl(iTrl)).*sEMG.scale.*1e6; %data unit ??? TO DO : no info in sEMG !!!
                    sData(iStim).vFs(iTrl) = Fs; % Fs
                    sData(iStim).ceTime{iTrl} = vTime; % vecteur temps
                    sData(iStim).vOffset(iTrl) = abs(vWin(1)); % offset en sample
                    sData(iStim).ceFile{iTrl} = ['<HTML><BODY color="black">' sEMG.title '-' sprintf('%03d',iTrl)]; % Nom fichier EMG
                    sData(iStim).vOrder(iTrl) = iTrl; %Ordre chrono de l'EMG
                end
                
            case 'Sp2_bis'
        

                % Sampling freq
                Fs = 1/wave_data.interval;
                
%                 % TO DO !!!
%                 % Time window limits : from -1000 to +2000ms by
%                 % default + TMS pulse @1.05 !!!
%                 vWin = floor([-0.3 0.3].*Fs);
%                 vTime = [-0.3 : 1/Fs : 0.3];

                
                % Filling cortextool data structure
                for iTrl = 1 : wave_data.frames
                    
                    % !!!!! inversion signal !!! TO DO !!! -300 / 300 ms :
                    % %                     sData(iStim).ceData{iTrl} = -wave_data.values([vWin(1) : vWin(2)]+1.05*Fs,iEMGChan,iTrl).*1e3; %data unit ??? TO DO : no info in sEMG !!!
                    % % sData(iStim).vOffset(iTrl) = abs(vWin(1)); % offset en sample
                    
                    % All:
                    sData(iStim).ceData{iTrl} = -wave_data.values(:,iEMGChan,iTrl).*1e3;
                    sData(iStim).vFs(iTrl) = Fs; % Fs
                    sData(iStim).ceTime{iTrl} = [0 : numel(sData(iStim).ceData{iTrl})-1]./Fs; % vecteur temps
                    sData(iStim).vOffset(iTrl) = abs(0); % offset en sample
                    sData(iStim).ceFile{iTrl} = ['<HTML><BODY color="black">' wave_data.frameinfo(iTrl).label '-' sprintf('%03d',iTrl)]; % Nom fichier EMG
                    sData(iStim).vOrder(iTrl) = iTrl; %Ordre chrono de l'EMG
                end
        
            case 'edf'
                            
                % Sampling freq
                Fs = sHdr.samples(iEMGChan);
                
                % Finding trial onsets
                switch TrigMode
                    case 1 % digital trig (= event name) without zeroing between event
                        vTrl = find(diff(mData(iTrigChan,:))~=0);
                    case 2 % simple boolean trig 
                        vTrl = find(mData(iTrigChan,:)~=0);
                end
                
                % TO DO !!!
                % Time window limits : from -200 to +300ms by
                % default
                vWin = floor([-0.2 0.3].*Fs);
                vTime = [-0.2 : 1/Fs : 0.3];
                
                % Amplitude factor
                switch sHdr.units{iEMGChan}
                    case 'V'
                        Amp = 1e6;
                    case 'mV'
                        Amp = 1e3;
                    case 'uV'
                        Amp = 1;
                end
                
                % Re-referecing if needed
                if ~isempty(iRefChan),  mData(iEMGChan,:) = mData(iEMGChan,:)-mData(iRefChan,:);   end
                
                % Filling cortextool data structure
                bFirst = 1;
                switch TrigMode
                    case 1 % digital trig (= event name) without zeroing between event
                        % sans protocol file !!!
                        for iTrl = 1 : numel(vTrl)
                            % check si event selectionne
                            if ~isempty(viEvents) && isempty(find(viEvents==mData(iTrigChan,vTrl(iTrl)+1))), continue; end
                            % premier passage : init
                            if iStim==1 && bFirst
                                sData_tmp.cEMGChan = sData.cEMGChan;
                                sData_tmp(1).Side = -1;
                                sData_tmp(1).cName = [cDirName '_' num2str(mData(iTrigChan,vTrl(iTrl)+1))];
                                sData_tmp(1).ceData{1} = mData(iEMGChan,[vWin(1) : vWin(2)]+vTrl(iTrl))'.*Amp; %data -> in µV
                                sData_tmp(1).vFs(1) = Fs; % Fs
                                sData_tmp(1).ceTime{1} = vTime; % vecteur temps
                                sData_tmp(1).vOffset(1) = abs(vWin(1)); % offset en sample
                                sData_tmp(1).ceFile{1} = ['<HTML><BODY color="black">' sHdr.label{iEMGChan} '-' sprintf('%03d',iTrl)]; % Nom fichier EMG
                                sData_tmp(1).vOrder(1) = 1; %Ordre chrono de l'EMG
                                bFirst = 0;
                                
                             % si condition n'existe pas encore, on la créé
                            elseif isempty(find(strcmp({sData_tmp.cName},[cDirName '_' num2str(mData(iTrigChan,vTrl(iTrl)+1))])))
                                sData_tmp(end+1).Side = -1;
                                sData_tmp(end).cName = [cDirName '_' num2str(mData(iTrigChan,vTrl(iTrl)+1))];
                                sData_tmp(end).ceData{1} = mData(iEMGChan,[vWin(1) : vWin(2)]+vTrl(iTrl))'.*Amp; %data -> in µV
                                sData_tmp(end).vFs(1) = Fs; % Fs
                                sData_tmp(end).ceTime{1} = vTime; % vecteur temps
                                sData_tmp(end).vOffset(1) = abs(vWin(1)); % offset en sample
                                sData_tmp(end).ceFile{1} = ['<HTML><BODY color="black">' sHdr.label{iEMGChan} '-' sprintf('%03d',iTrl)]; % Nom fichier EMG
                                sData_tmp(end).vOrder(1) = iTrl; %Ordre chrono de l'EMG
                            
                                % si condition existe déjà
                            else
                                iCond = find(strcmp({sData_tmp.cName},[cDirName '_' num2str(mData(iTrigChan,vTrl(iTrl)+1))]));
                                
                                sData_tmp(iCond).ceData{end+1} = mData(iEMGChan,[vWin(1) : vWin(2)]+vTrl(iTrl))'.*Amp; %data -> in µV
                                sData_tmp(iCond).vFs(end+1) = Fs; % Fs
                                sData_tmp(iCond).ceTime{end+1} = vTime; % vecteur temps
                                sData_tmp(iCond).vOffset(end+1) = abs(vWin(1)); % offset en sample
                                sData_tmp(iCond).ceFile{end+1} = ['<HTML><BODY color="black">' sHdr.label{iEMGChan} '-' sprintf('%03d',iTrl)]; % Nom fichier EMG
                                sData_tmp(iCond).vOrder(end+1) = iTrl; %Ordre chrono de l'EMG
                                
                            end
                        end
                        
                        
                    case 2
                        for iTrl = 1 : numel(vTrl)
                            sData(iStim).ceData{iTrl} = mData(iEMGChan,[vWin(1) : vWin(2)]+vTrl(iTrl))'.*Amp; %data -> in µV
                            sData(iStim).vFs(iTrl) = Fs; % Fs
                            sData(iStim).ceTime{iTrl} = vTime; % vecteur temps
                            sData(iStim).vOffset(iTrl) = abs(vWin(1)); % offset en sample
                            sData(iStim).ceFile{iTrl} = ['<HTML><BODY color="black">' sHdr.label{iEMGChan} '-' sprintf('%03d',iTrl)]; % Nom fichier EMG
                            sData(iStim).vOrder(iTrl) = iTrl; %Ordre chrono de l'EMG
                        end
                end
                
            case 'mat'
                            
                % Sampling freq
                switch isi_units
                    case 's'
                        Fs = 1/isi;
                    case 'ms'
                        Fs = 1/(isi*10^-3);
                end
                
                % Finding trial onsets
                vPosTrig = find(mData(iTrigChan,:)>2.5); %!!! if +5 Volts !!!
                vTrl = [vPosTrig(1) vPosTrig(find(diff(vPosTrig)>1)+1)];
                
                % TO DO !!!
                % Time window limits : from -200 to +300ms by
                % default
                vWin = floor([-0.2 0.3].*Fs);
                vTime = [-0.2 : 1/Fs : 0.3];
                
                % Amplitude factor
                switch units(iEMGChan,:)
                    case 'Volts'
                        Amp = 1e3;
                    otherwise
                        error('Non recognized units. Please contact sylvain dot harquel at univ-grenoble-alpes dot fr');
                end
                  
                % Filling cortextool data structure
                for iTrl = 1 : numel(vTrl)
                    
                    sData(iStim).ceData{iTrl} = mData(iEMGChan,[vWin(1) : vWin(2)]+vTrl(iTrl))'.*Amp; %data -> in µV
                    sData(iStim).vFs(iTrl) = Fs; % Fs
                    sData(iStim).ceTime{iTrl} = vTime; % vecteur temps
                    sData(iStim).vOffset(iTrl) = abs(vWin(1)); % offset en sample
                    sData(iStim).ceFile{iTrl} = ['<HTML><BODY color="black">' deblank(ceChanName{iEMGChan}) '-' sprintf('%03d',iTrl)]; % Nom fichier EMG
                    sData(iStim).vOrder(iTrl) = iTrl; %Ordre chrono de l'EMG
                end
                
            case 'LabV'
                sFiles = dir([cPath_tmp]);
                for iFile = 3 : numel(sFiles)
                    sTmp = load([cPath_tmp filesep sFiles(iFile).name]);
                    sData(iStim).ceData{iFile-2} = sTmp(:,1).*1e3;
                    sData(iStim).vFs(iFile-2) = 5000; %TO DO !!!
                    sData(iStim).ceTime{iFile-2} = [1:4000]./5000 - 0.1; %TO DO !!!
                    sData(iStim).vOffset(iFile-2) = 500; %TO DO !!!
                    sData(iStim).ceFile{iFile-2} = ['<HTML><BODY color="black">' deblank(sFiles(iFile).name)]; % Nom fichier EMG
                    sData(iStim).vOrder(iFile-2) = iFile-2;
                end

        end
        
        % Signal polarity
%         vAll = [];
%         for iTrl = 1 : numel(sData(iStim).ceData), vAll = [vAll sData(iStim).ceData{iTrl}'];  end
%         [vPeaks viPeaks] = findpeaks(vAll);
%         [i viSortedP] = sort(vPeaks);
%         if sum(sign(vAll(viPeaks(viSortedP(end-10:end))))) > 5, 
%             for iTrl = 1 : numel(sData(iStim).ceData), sData(iStim).ceData{iTrl} = -sData(iStim).ceData{iTrl};  end       
%         end
       
        % Next stim (condition)
        iStim = iStim+1;
        
    else
        continue;
    end
end

% si dernier passage, on remplace sData
if exist('sData_tmp'), sData = sData_tmp; end








