function [hFig] = PlotBar(vMean, vStd, vColor, ceXLabel, vP, hFig, vXvalues, cYlabel)

% Fonction hFig = PlotBar(vMean, vStd, vColor, ceXLabel, vP, hFig)
%
% Plots bars of mean and std values
%
% INPUTS :  - vMean:[VECTOR] contains the means to plot
%           - vStd:[VECTOR] contains the std to plot
%           - vColor:[VECTOR] string vector containing the colors to be
%           used for each mean/std couple
%           - ceXLabel:[CELL] contains the labels of the X axis
%           - vP:[VECTOR, opt.] contains p-values
%           - hFig:[HANDLE, opt.] figure handle to use for plotting
%           - vXvalues:[VECTOR] contains the X coordinates
%           - cYlabel:[STRING] Y axis label
%
% OUTPUT : - hFig:[HANDLE] returns figure handle

% Copyright (C) 2015  Sylvain Harquel, CNRS
%
% This file is part of CortExTool.
% 
% CortExTool is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% CortExTool is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with CortExTool.  If not, see <http://www.gnu.org/licenses/>

if ~exist('vP'), vP =[]; end
if ~exist('hFig') || isempty(hFig), hFig = -1; end
if ~exist('vXvalues') || isempty(vXvalues), vXvalues = [1:numel(vMean)]; end


if hFig == -1 
   hFig = figure;
else
   figure(hFig); 
end
hold on,
for i = 1 : numel(vMean)
    if numel(vColor) > 1, cColor = vColor(i); else cColor = vColor; end
    plot(vXvalues(i),vMean(i),[cColor 'o-'],'MarkerSize',8),
    plot([vXvalues(i),vXvalues(i)],[vMean(i)+vStd(i) vMean(i)],'k');
    plot([vXvalues(i),vXvalues(i)],[vMean(i) vMean(i)-vStd(i)],'k');
    plot([vXvalues(i)-0.1,vXvalues(i)+0.1],[vMean(i)+vStd(i) vMean(i)+vStd(i)],'k');
    plot([vXvalues(i)-0.1,vXvalues(i)+0.1],[vMean(i)-vStd(i) vMean(i)-vStd(i)],'k');
    if (~isempty(vP) && (vP(i) == 1))
       plot(vXvalues(i),vMean(i)+vStd(i) + 0.2,'*k');
    end
end
plot(vXvalues,vMean,'k--'),
set(gca,'XTick',vXvalues,'FontSize',10);

for iLabel = 1 : numel(ceXLabel)
    if ~isempty(strfind(ceXLabel{iLabel},'<HTML>'))
        ceXLabel{iLabel} = ceXLabel{iLabel}(max(strfind(ceXLabel{iLabel},'>'))+1 : end);
    end
end
set(gca,'XTickLabel',ceXLabel);
set(gca,'FontSize',10);
ylabel(cYlabel,'FontSize',10);

% if (~isempty(vP) && (vP(7) == 1))
%     y = max(vMean(2)+vStd(2),vMean(4)+vStd(4)) + 0.4;
%     plot([2 4],[y y],'k--');
% end
% if (~isempty(vP) && (vP(8) == 1))
%     y = max(vMean(4)+vStd(4),vMean(6)+vStd(6)) + 0.5;
%     plot([4 6],[y y],'k--');
% end
% if (~isempty(vP) && (vP(9) == 1))
%     y = max(vMean(2)+vStd(2),vMean(6)+vStd(6)) + 0.6;
%     plot([2 6],[y y],'k--');
% end